/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import control.Control;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import model.Aula;
import model.Curso;
import model.HoraAula;
import model.Turma;
import utils.GenericTableModel;

/**
 *
 * @author prvilela
 */
public class RelatorioAulasDadasTableModel extends GenericTableModel {

    DateFormat dateFormat;

    public RelatorioAulasDadasTableModel(ArrayList vDados) {
        // Use esse vetor de Strings para definir os titulos das colunas:
        super(vDados, new String[]{"Data", "Curso", "Turma", "Carga horária", "R$"});
        dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return String.class;
            case 1:
                return String.class;
            case 2:
                return String.class;
            case 3:
                return Double.class;
            case 4:
                return Double.class;
            default:
                throw new IndexOutOfBoundsException("columnIndex out of bounds");
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Aula aula = (Aula) vDados.get(rowIndex);

        switch (columnIndex) {
            case 0:
                return dateFormat.format(aula.getData().getTime());
            case 1:
                return ((Curso) Control.getCursoById(((Turma) Control.getTurmaByIdTurma(aula.getIdTurma())).getIdCurso())).getNome();
            case 2:
                return ((Turma) Control.getTurmaByIdTurma(aula.getIdTurma())).getDescricao();
            case 3:
                return (aula.getCargaHoraria() / 60.0);
            case 4:
                double horas = (aula.getCargaHoraria() / 60.0);
                HoraAula horaAula = ((HoraAula) Control.getHoraAulaByIdTurmaIdProfessor(aula.getIdTurma(), aula.getIdProfessor()));
                double valor = 0;
                if (horaAula != null) {
                    valor = horaAula.getValor();
                }
                return horas * valor * 1.0;
            default:
                throw new IndexOutOfBoundsException("columnIndex out of bounds");
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }
}
