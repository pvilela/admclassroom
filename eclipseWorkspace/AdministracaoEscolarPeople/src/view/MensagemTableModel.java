package view;

import control.Control;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import model.Mensagem;
import utils.GenericTableModel;

/**
 *
 * @author prvilela
 */
public class MensagemTableModel extends GenericTableModel {
    DateFormat dateFormat;
    
    public MensagemTableModel(ArrayList vDados) {
        // Use esse vetor de Strings para definir os titulos das colunas:
        super(vDados, new String[]{"Lida","De:", "Para:", "Data", "Assunto"});
        dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return Boolean.class;
            case 1:
                return String.class;
            case 2:
                return String.class;
            case 3:
                return String.class;
            case 4:
                return String.class;
            default:
                throw new IndexOutOfBoundsException("columnIndex out of bounds");
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Mensagem mensagem = (Mensagem) vDados.get(rowIndex);

        switch (columnIndex) {
            case 0:
                switch (mensagem.getStatus()) {
                    case 0:
                        return false;
                    case 1:
                        return true;
                    default:
                        return false;
                }
            case 1:
                return Control.getUsuarioById(mensagem.getIdUsuarioOrigem());
            case 2:
                return Control.getUsuarioById(mensagem.getIdUsuarioDestino());
            case 3:
                return dateFormat.format((mensagem.getDataHora().getTime()));
            case 4:
                return mensagem.getAssunto();
            default:
                throw new IndexOutOfBoundsException("columnIndex out of bounds");
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }
}
