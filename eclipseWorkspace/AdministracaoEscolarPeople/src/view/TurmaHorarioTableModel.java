/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import control.Control;
import java.util.ArrayList;
import model.TurmaHorario;
import utils.GenericTableModel;

/**
 *
 * @author prvilela
 */
public class TurmaHorarioTableModel extends GenericTableModel {

    public TurmaHorarioTableModel(ArrayList vDados) {
        // Use esse vetor de Strings para definir os titulos das colunas:
        super(vDados, new String[]{"Dia da Semana", "Horário", "C/H"});
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return String.class;
            case 1:
                return String.class;
            case 2:
                return String.class;
            default:
                throw new IndexOutOfBoundsException("columnIndex out of bounds");
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        TurmaHorario turmaHorario = (TurmaHorario) vDados.get(rowIndex);

        switch (columnIndex) {
            case 0:
                switch (turmaHorario.getDiaDaSemana()) {
                    case 0:
                        return "Domingo";
                    case 1:
                        return "Segunda";
                    case 2:
                        return "Terça";
                    case 3:
                        return "Quarta";
                    case 4:
                        return "Quinta";
                    case 5:
                        return "Sexta";
                    case 6:
                        return "Sábado";
                    default:
                        return "";
                }
            case 1:
                return turmaHorario.getStrHorario();
            case 2:
                return new String(turmaHorario.getCargaHoraria()/60.0+"");
            default:
                throw new IndexOutOfBoundsException("columnIndex out of bounds");
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        TurmaHorario turmaHorario = (TurmaHorario) vDados.get(rowIndex);
        switch(columnIndex){
            case 2:
                turmaHorario.setCargaHoraria(((Integer)aValue).intValue());
                Control.updateTurmaHorario(turmaHorario);
                this.fireTableDataChanged();
                break;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        if(columnIndex==2){
            return true;
        }
        return false;
    }
}
