package view;

import control.Control;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Aula;
import model.Turma;
import utils.GenericTableModel;

/**
 *
 * @author prvilela
 */
public class AulasTableModel extends GenericTableModel {

    DateFormat dateFormat;
    boolean editable;

    public AulasTableModel(ArrayList vDados, boolean editable) {
        // Use esse vetor de Strings para definir os titulos das colunas:
        super(vDados, new String[]{"Número", "Data", "Conteúdo Ministrado", "Conteúdo Previsto", "CH-aula", "CH-acumulada", "Ocorreu?"});
        dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        this.editable = editable;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return Integer.class;
            case 1:
                return String.class;
            case 2:
                return String.class;
            case 3:
                return String.class;
            case 4:
                return Double.class;
            case 5:
                return Double.class;
            case 6:
                return Boolean.class;
            default:
                throw new IndexOutOfBoundsException("columnIndex out of bounds");
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Aula aula = (Aula) vDados.get(rowIndex);
        double hrs = 0;
        for (int i = 0; i <= rowIndex; i++) {
            Aula a = (Aula) vDados.get(i);
            if (a.getStatus() == 1) {
                hrs += (a.getCargaHoraria() / 60.0);
            }
        }

        switch (columnIndex) {
            case 0:
                return aula.getNumero();
            case 1:
                return dateFormat.format((aula.getData().getTime()));
            case 2:
                return aula.getConteudoMinistrado();
            case 3:
                return aula.getConteudoPrevisto();
            case 4:
                return ((aula.getStatus() == 2) ? 0 : (aula.getCargaHoraria() / 60.0));
            case 5:
                return hrs;
            case 6:
                return ((aula.getStatus() == 1) ? true : false);
            default:
                throw new IndexOutOfBoundsException("columnIndex out of bounds");
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        try {
            Aula aula = (Aula) vDados.get(rowIndex);
            switch (columnIndex) {
                case 0:
                    aula.setNumero((Integer) aValue);
                    break;
                case 1:
                    Calendar dt = Calendar.getInstance();
                    dt.setTime((Date) dateFormat.parse((String) aValue));
                    aula.setData(dt);
                    break;
                case 2:
                    aula.setConteudoMinistrado((String) aValue);
                    break;
                case 3:
                    aula.setConteudoPrevisto((String) aValue);
                    break;
                case 4:
                    int ch = (int) ((Double) aValue * 60);
                    aula.setCargaHoraria(ch);
                    this.fireTableDataChanged();
                    break;
                case 5:
                    break;
                case 6:
                    aula.setStatus((((Boolean) aValue) ? 1 : 0));
                    break;
                default:
                    throw new IndexOutOfBoundsException("columnIndex out of bounds");
            }
            Control.updateAula(aula);
        } catch (ParseException ex) {
            Logger.getLogger(AulasTableModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        Aula aula = (Aula) vDados.get(rowIndex);
        if (aula.getStatus() == 2) {
            return false;
        }
        if (columnIndex == 5) {
            return false;
        }
        return editable;
    }
}
