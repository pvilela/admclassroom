package view;

import control.Control;
import java.util.ArrayList;
import model.AlunoAvaliacao;
import model.Avaliacao;
import model.Presenca;
import utils.GenericTableModel;

/**
 *
 * @author prvilela
 */
public class PresencasTableModel extends GenericTableModel {

    public PresencasTableModel(ArrayList vDados) {
        // Use esse vetor de Strings para definir os titulos das colunas:
        super(vDados, new String[]{"Nome", "Observações", "Presente?"});
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return String.class;
            case 1:
                return String.class;
            case 2:
                return Boolean.class;
            case 3:
                return Double.class;
            default:
                throw new IndexOutOfBoundsException("columnIndex out of bounds");
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Presenca presenca = (Presenca) vDados.get(rowIndex);
        Avaliacao avaliacao = Control.getAvaliacaoByIdAula(presenca.getIdAula());
        double nota = 0.0;
        if (avaliacao != null) {
            AlunoAvaliacao alunoAvaliacao = Control.getAlunoAvaliacao(presenca.getIdAluno(), avaliacao.getId());
            nota = alunoAvaliacao.getNota();
        }

        switch (columnIndex) {
            case 0:
                return Control.getPessoaById(presenca.getIdAluno());
            case 1:
                return presenca.getObservacao();
            case 2:
                return ((presenca.getPresenca() == 1) ? true : false);
            case 3:
                return nota;
            default:
                throw new IndexOutOfBoundsException("columnIndex out of bounds");
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        Presenca presenca = (Presenca) vDados.get(rowIndex);
        Avaliacao avaliacao = Control.getAvaliacaoByIdAula(presenca.getIdAula());
        switch (columnIndex) {
            case 0:
                break;
            case 1:
                presenca.setObservacao((String) aValue);
                break;
            case 2:
                presenca.setPresenca((((Boolean) aValue) ? 1 : 0));
                break;
            case 3:
                if (avaliacao != null) {
                    AlunoAvaliacao alunoAvaliacao = Control.getAlunoAvaliacao(presenca.getIdAluno(), avaliacao.getId());
                    alunoAvaliacao.setNota(((Double) aValue));
                    Control.updateAlunoAvaliacao(alunoAvaliacao);
                }
                break;
            default:
                throw new IndexOutOfBoundsException("columnIndex out of bounds= ");
        }
        Control.updatePresenca(presenca);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        if (columnIndex > 0) {
            return true;
        }
        return false;
    }

    @Override
    public void addListOfItems(ArrayList<Object> vItens) {
        super.addListOfItems(vItens);
        if (!vDados.isEmpty()) {
            Presenca presenca = (Presenca) vDados.get(0);
            if (presenca != null) {
                Avaliacao avaliacao = Control.getAvaliacaoByIdAula(presenca.getIdAula());
                if (avaliacao != null) {
                    super.colunas = new String[]{"Nome", "Observações", "Presente?", "Nota"};
                } else {
                    super.colunas = new String[]{"Nome", "Observações", "Presente?"};
                }
            }
        } else {
            super.colunas = new String[]{"Nome", "Observações", "Presente?"};
        }
        this.fireTableStructureChanged();
    }
}
