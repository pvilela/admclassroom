/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import control.Control;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import model.Pessoa;
import model.Turma;
import utils.GenericTableModel;

/**
 *
 * @author prvilela
 */
public class TurmasTableModel extends GenericTableModel {
    DateFormat dateFormat;

    public TurmasTableModel(ArrayList vDados) {
        // Use esse vetor de Strings para definir os titulos das colunas:
        super(vDados, new String[]{"Curso","Descrição", "Carga Horária", "Professor", "Data Iinicio", "Data Fim", "Status"});
        //super(vDados, new String[]{"Curso","Descrição", "Carga Horária", "Professor", "Data Iinicio", "Data Fim", "Status", "Observações"});
        dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return String.class;
            case 1:
                return String.class;
            case 2:
                return Integer.class;
            case 3:
                return String.class;
            case 4:
                return String.class;
            case 5:
                return String.class;
            case 6:
                return String.class;
            //case 7:
            //    return String.class;
            default:
                throw new IndexOutOfBoundsException("columnIndex out of bounds");
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Turma turma = (Turma) vDados.get(rowIndex);
        Pessoa professor = (Pessoa)Control.getPessoaById(turma.getIdProfessor());

        switch (columnIndex) {
            case 0:
                return (Control.getCursoById(turma.getIdCurso())).getNome();
            case 1:
                return turma.getDescricao();
            case 2:
                return Control.getCargaHorariaTotal(turma.getIdCurso());
            case 3:
                return (professor!=null?professor.getNome():"");
            case 4:
                return dateFormat.format((turma.getDtInicio().getTime()));
            case 5:
                return dateFormat.format((turma.getDtFim().getTime()));
            case 6:
                switch(turma.getStatus()){
                    case 0:
                        return "Não iniciou";
                    case 1:
                        return "Em andamento";
                    case 2:
                        return "Encerrou";
                    default:
                        return "";
                }
            //case 7:
              //  return turma.getObservacoes();
            default:
                throw new IndexOutOfBoundsException("columnIndex out of bounds");
        }
    }


    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }
}
