package view;

import control.Control;
import java.util.ArrayList;
import model.Modulo;
import utils.GenericTableModel;

/**
 *
 * @author prvilela
 */
public class ModulosTableModel extends GenericTableModel {

    public ModulosTableModel(ArrayList vDados) {
        // Use esse vetor de Strings para definir os titulos das colunas:
        super(vDados, new String[]{"Nome do Módulo", "Carga Horária"});
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return String.class;
            case 1:
                return Integer.class;
            default:
                throw new IndexOutOfBoundsException("columnIndex out of bounds");
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Modulo modulo = (Modulo) vDados.get(rowIndex);

        switch (columnIndex) {
            case 0:
                return modulo.getNome();
            case 1:
                return modulo.getCargaHoraria();
            default:
                throw new IndexOutOfBoundsException("columnIndex out of bounds");
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
         Modulo modulo = (Modulo) vDados.get(rowIndex);
        switch (columnIndex) {
            case 0:
                modulo.setNome((String) aValue);
                break;
            case 1:
                modulo.setCargaHoraria((Integer) aValue);
                break;
            default:
                throw new IndexOutOfBoundsException("columnIndex out of bounds");
        }
        Control.updateModulo(modulo);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return true;
    }
}
