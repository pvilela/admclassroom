package view;

import control.Control;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import model.Pessoa;
import utils.GenericTableModel;

/**
 *
 * @author prvilela
 */
public class PessoasTableModel extends GenericTableModel {

    public PessoasTableModel(ArrayList vDados) {
        // Use esse vetor de Strings para definir os titulos das colunas:
        super(vDados, new String[]{"Nome", "Email", "Telefone", "Tipo", "Responsável", "Ativo"});
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return String.class;
            case 1:
                return String.class;
            case 2:
                return String.class;
            case 3:
                return String.class;
            case 4:
                return String.class;
            case 5:
                return Boolean.class;
            default:
                throw new IndexOutOfBoundsException("columnIndex out of bounds");
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Pessoa pessoa = (Pessoa) vDados.get(rowIndex);

        switch (columnIndex) {
            case 0:
                return pessoa.getNome();
            case 1:
                return pessoa.getEmail();
            case 2:
                return pessoa.getTelefone();
            case 3:
                switch (pessoa.getTipo()) {
                    case 0:
                        return "Funcionário";
                    case 1:
                        return "Professor";
                    case 2:
                        return "Direção";
                    case 3:
                        return "Sys admin";
                    case 4:
                        return "Aluno";
                    case 5:
                        return "Responsável";
                    default:
                        return "";
                }
            case 4:
                if (pessoa.getIdResponsavel() != 0) {
                    return Control.getPessoaById(pessoa.getIdResponsavel()).getNome();
                }
                return "";
            case 5:
                return ((pessoa.getStatus() == 1) ? true : false);
            default:
                throw new IndexOutOfBoundsException("columnIndex out of bounds");
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        Pessoa pessoa = (Pessoa) vDados.get(rowIndex);
        switch (columnIndex) {
            case 0:
                pessoa.setNome((String) aValue);
                break;
            case 1:
                pessoa.setEmail((String) aValue);
                break;
            case 2:
                pessoa.setTelefone((String) aValue);
                break;
            case 3:
                pessoa.setTipo((Integer) aValue);
                break;
            case 4:
                pessoa.setIdResponsavel((Integer) aValue);
                break;
            case 5:
                if((Boolean) aValue){
                    pessoa.setStatus(1);
                }else{
                    int resp = JOptionPane.showConfirmDialog(null, "Ao inativar " + pessoa.getNome() + " ela será\nremovida de todas as turmas que faz parte.\nConfirma?");
                    if(resp==0){
                        pessoa.setStatus(0);
                        Control.removeAlunoAllTurmas(pessoa.getId());
                    }
                }
                //pessoa.setStatus(((Boolean) aValue ? 1 : 0));
                break;
            default:
                throw new IndexOutOfBoundsException("columnIndex out of bounds");
        }
        Control.updatePessoa(pessoa);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        if ((columnIndex < 3) || (columnIndex == 5)) {
            return true;
        }
        return false;
    }
}
