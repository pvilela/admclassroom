package control;

import java.util.ArrayList;
import java.util.Calendar;
import model.AlunoAvaliacao;
import model.AlunoAvaliacaoDAO;
import model.AlunoTurma;

import model.AlunoTurmaDAO;
import model.Aula;
import model.AulaDAO;
import model.Avaliacao;
import model.AvaliacaoDAO;
import model.Curso;
import model.CursoDAO;
import model.CursoModuloDAO;
import model.HoraAula;
import model.HoraAulaDAO;
import model.Mensagem;
import model.MensagemDAO;
import model.Modulo;
import model.ModuloDAO;
import model.Pessoa;
import model.PessoaDAO;
import model.Presenca;
import model.PresencaDAO;
import model.Sistema;
import model.SistemaDAO;
import model.TipoCurso;
import model.TipoCursoDAO;
import model.Turma;
import model.TurmaDAO;
import model.TurmaHorario;
import model.TurmaHorarioDAO;
import model.Usuario;
import model.UsuarioDAO;

/**
 * @author Prof. Dr. Plinio Vilela - vilela@ydoo.com.br
 */
public class Control {

    // - Controle da Versao
    public static Sistema getVersaoAtual() {
        return SistemaDAO.getInstance().retrieveLastId();
    }

    public static void updateVersaoAtual(int versao, String observacoes) {
        SistemaDAO.getInstance().create(versao, observacoes);
    }

    //- Control Pessoas:
    public static Pessoa createPessoa(int tipo, String nome) {
        PessoaDAO.getInstance().create(tipo, nome);
        return PessoaDAO.getInstance().retrieveLastId();
    }

    public static Pessoa getPessoaById(int id) {
        return PessoaDAO.getInstance().retrieveById(id);
    }

    public static ArrayList<Object> getAllPessoas() {
        return PessoaDAO.getInstance().retrieveAll();
    }

    public static ArrayList<Object> getAllPessoasByTipo(int tipo) {
        return PessoaDAO.getInstance().retrieveAllByTipo(tipo);
    }

    public static ArrayList<Object> getAllPessoasAtivos() {
        return PessoaDAO.getInstance().retrieveAllAtivos();
    }

    public static ArrayList<Object> getAllPessoasAtivosByTipo(int tipo) {
        return PessoaDAO.getInstance().retrieveAllAtivosByTipo(tipo);
    }

    public static ArrayList<Object> getAllPessoasLike(String nome) {
        return PessoaDAO.getInstance().retrieveLike(nome);
    }

    public static void updatePessoa(Pessoa p) {
        PessoaDAO.getInstance().update(p);
    }

    public static ArrayList<Object> getAllAlunosAtivosNotInTurma(int idTurma) {
        return PessoaDAO.getInstance().retrieveAlunosAtivosNotInTurma(idTurma);
    }

    public static ArrayList<Object> getAllAlunosByIdTurma(int idTurma) {
        return AlunoTurmaDAO.getInstance().retrieveAlunosByIdTurma(idTurma);
    }

    //- Control Usuarios:
    public static Usuario createUsuario(int idPessoa, String username) {
        UsuarioDAO.getInstance().create(idPessoa, username, "");
        return UsuarioDAO.getInstance().retrieveLastId();
    }

    public static ArrayList<Object> getAllUsuarios() {
        return UsuarioDAO.getInstance().retrieveAll();
    }

    public static ArrayList<Object> getAllUsuariosAtivos() {
        return UsuarioDAO.getInstance().retrieveAllAtivo();
    }    
    
    public static Usuario getUsuarioByIdPessoa(int idPessoa) {
        return UsuarioDAO.getInstance().retrieveByIdPessoa(idPessoa);
    }

    public static boolean checkPasswordUsuario(String username, String password) {
        return UsuarioDAO.getInstance().checkPassword(username, password);
    }

    public static Usuario getUsuarioById(int idUsuario) {
        return UsuarioDAO.getInstance().retrieveById(idUsuario);
    }

    public static Usuario getUsuarioByUsername(String username) {
        return UsuarioDAO.getInstance().retrieveByUsername(username);
    }

    public static void updateUsuario(Usuario usuario) {
        UsuarioDAO.getInstance().update(usuario);
    }

    public static void updateSenhaUsuario(Usuario usuario) {
        UsuarioDAO.getInstance().updateSenha(usuario.getId(), usuario.getSenha());
    }

    //- Control Cursos:
    public static Curso createCurso(int idTipoCurso, String nome) {
        CursoDAO.getInstance().create(idTipoCurso, nome);
        return CursoDAO.getInstance().retrieveLastId();
    }

    public static ArrayList<Object> getAllCursos() {
        return CursoDAO.getInstance().retrieveAll();
    }

    public static Curso getCursoById(int idCurso) {
        return CursoDAO.getInstance().retrieveById(idCurso);
    }

    public static int getCargaHorariaTotal(int idCurso) {
        ArrayList<Object> vModulos = CursoModuloDAO.getInstance().retrieveAllModuloByIdCurso(idCurso);
        int cargaHorariaTotal = 0;
        for (Object o : vModulos) {
            cargaHorariaTotal += ((Modulo) o).getCargaHoraria();
        }
        return cargaHorariaTotal;
    }

    public static void updateCurso(Curso curso) {
        CursoDAO.getInstance().update(curso);
    }

    public static void deleteCurso(Curso curso) {
        CursoDAO.getInstance().delete(curso);
    }

    //- Control TipoDeCurso:
    public static TipoCurso createTipoDeCurso(String tipoDeCurso) {
        TipoCursoDAO.getInstance().create(tipoDeCurso);
        return TipoCursoDAO.getInstance().retrieveLastId();
    }

    public static ArrayList<Object> getAllTipoCursos() {
        return TipoCursoDAO.getInstance().retrieveAll();
    }

    public static TipoCurso getTipoCursoById(int idTipoCurso) {
        return TipoCursoDAO.getInstance().retrieveById(idTipoCurso);
    }

    public static boolean deleteTipoCurso(TipoCurso tipoCurso) {
        // Deleta apenas se o TipoCurso nao estiver associado a nenhum curso.
        ArrayList<Object> vCursos = CursoDAO.getInstance().retrieveByTipoCurso(tipoCurso.getId());
        if (vCursos.isEmpty()) {
            TipoCursoDAO.getInstance().delete(tipoCurso);
            return true;
        } else {
            for (Object o : vCursos) {
                System.out.println(((Curso) o).getNome());
            }
        }
        return false;
    }

    //- Control Modulo:
    public static Modulo createModulo(String nome, int cargaHoraria) {
        ModuloDAO.getInstance().create(nome, cargaHoraria);
        return ModuloDAO.getInstance().retrieveLastId();
    }

    public static ArrayList<Object> getAllModulosByIdCurso(int idCurso) {
        return CursoModuloDAO.getInstance().retrieveAllModuloByIdCurso(idCurso);
    }

    public static ArrayList<Object> getAllModulos() {
        return ModuloDAO.getInstance().retrieveAll();
    }

    public static boolean deleteModulo(Modulo modulo) {
        // Deleta apenas se o modulo nao estiver associado a nenhum curso
        if (CursoModuloDAO.getInstance().isModuloBeingUsed(modulo.getId())) {
            return false;
        } else {
            ModuloDAO.getInstance().delete(modulo);
            return true;
        }
    }

    public static void updateModulo(Modulo modulo) {
        ModuloDAO.getInstance().update(modulo);
    }

    //- Control CursoModulo
    public static void createCursoModulo(int idCurso, int idModulo) {
        CursoModuloDAO.getInstance().create(idCurso, idModulo);
    }

    public static void deleteCursoModulo(int idCurso, int idModulo) {
        CursoModuloDAO.getInstance().delete(idCurso, idModulo);
    }

    //- Control TurmaHorario
    public static TurmaHorario createTurmaHorario(int idTurma, int diaDaSemana, int horario, int cargaHoraria) {
        TurmaHorarioDAO.getInstance().create(idTurma, diaDaSemana, horario, cargaHoraria);
        return TurmaHorarioDAO.getInstance().retrieveLastId();
    }

    public static ArrayList<Object> getAllTurmaHorarioByIdTurma(int idTurma) {
        return TurmaHorarioDAO.getInstance().retrieveByIdTurma(idTurma);
    }

    public static void updateTurmaHorario(TurmaHorario turmaHorario) {
        TurmaHorarioDAO.getInstance().update(turmaHorario);
    }

    public static void deleteTurmaHorario(TurmaHorario turmaHorario) {
        TurmaHorarioDAO.getInstance().delete(turmaHorario);
    }

    public static void deleteAllTurmaHorarioByIdTurma(int idTurma) {
        TurmaHorarioDAO.getInstance().deleteAllByIdTurma(idTurma);
    }

    public static int getCargaHorariaSemanal(int idTurma) {
        int cargaHorariaSemanal = 0;
        ArrayList<Object> vTurmaHorario = TurmaHorarioDAO.getInstance().retrieveByIdTurma(idTurma);
        for (Object o : vTurmaHorario) {
            cargaHorariaSemanal += ((TurmaHorario) o).getCargaHoraria();
        }
        return cargaHorariaSemanal;
    }

    //- Control Turma
    public static Turma createTurma(int idCurso, int status) {
        TurmaDAO.getInstance().create(idCurso, status);
        return TurmaDAO.getInstance().retrieveLastId();
    }

    public static ArrayList<Object> getAllTurmas() {
        return TurmaDAO.getInstance().retrieveAll();
    }

    public static Turma getTurmaByIdTurma(int idTurma) {
        return TurmaDAO.getInstance().retrieveById(idTurma);
    }

    public static ArrayList<Object> getTurmasByIdAluno(int idAluno) {
        return TurmaDAO.getInstance().retrieveByIdAluno(idAluno);
    }    
    
    public static ArrayList<Object> getAllTurmasByStatusBusca(int statusBusca) {
        return TurmaDAO.getInstance().retrieveByStatusBusca(statusBusca);
    }

    public static ArrayList<Object> getAllTurmasByStatusBuscaIdProfessor(int statusBusca, int idProfessor) {
        return TurmaDAO.getInstance().retrieveByStatusBuscaByIdProfessor(statusBusca, idProfessor);
    }

    public static ArrayList<Object> getAllTurmasByStatusBuscaLikeCurso(String nomeCurso, int statusBusca) {
        return TurmaDAO.getInstance().retrieveByStatusBuscaLikeCurso(nomeCurso, statusBusca);
    }

    public static ArrayList<Object> getAllTurmasByStatusBuscaLikeProfessor(String nomeProfessor, int statusBusca) {
        return TurmaDAO.getInstance().retrieveByStatusBuscaLikeProfessor(nomeProfessor, statusBusca);
    }

    public static void updateTurma(Turma turma) {
        TurmaDAO.getInstance().update(turma);
    }

    public static void deleteTurma(Turma turma) {
        TurmaDAO.getInstance().delete(turma);
    }

    private static int numSemanasDeAula(Turma turma) {
        int cargaHorariaTotal = Control.getCargaHorariaTotal(turma.getIdCurso()) * 60; //Transforma para minutos.
        int cargaHorariaSemanal = Control.getCargaHorariaSemanal(turma.getId());
        return ((cargaHorariaSemanal == 0) ? 0 : (cargaHorariaTotal / cargaHorariaSemanal));
    }

    public static Calendar getPrevisaoDataFim(Turma turma) {
        int nSemanas = numSemanasDeAula(turma);
        Calendar dtFinal = Calendar.getInstance();
        dtFinal.setTime(turma.getDtInicio().getTime());
        dtFinal.add(Calendar.DATE, (nSemanas * 7));
        return dtFinal;
    }

    //- Control Mensagem
    public static void createMensagem(int idUsuarioOrigem, int idUsuarioDestino, String assunto, String mensagem) {
        MensagemDAO.getInstance().create(idUsuarioOrigem, idUsuarioDestino, assunto, mensagem);
    }

    public static ArrayList<Object> getAllMensagensByIdUsuarioDestino(int idUsuarioDestino) {
        return MensagemDAO.getInstance().retrieveByUsuarioDestino(idUsuarioDestino);
    }

    public static ArrayList<Object> getAllMensagensByIdUsuarioOrigem(int idUsuarioOrigem) {
        return MensagemDAO.getInstance().retrieveByUsuarioOrigem(idUsuarioOrigem);
    }

    public static void updateMensagem(Mensagem mensagem) {
        MensagemDAO.getInstance().update(mensagem);
    }

    //- Control AlunoTurma
    public static void addAlunoTurma(int idAluno, int idTurma) {
        AlunoTurmaDAO.getInstance().create(idAluno, idTurma);
        createListaPresencaAlunoTurma(idAluno, idTurma);
    }

    public static void removeAlunoTurma(int idAluno, int idTurma) {
        AlunoTurmaDAO.getInstance().delete(idAluno, idTurma);
        deleteListaPresencaAlunoTurma(idAluno, idTurma);
    }

    public static void removeAlunoAllTurmas(int idAluno) {
        ArrayList<Object> vAlunoTurma = AlunoTurmaDAO.getInstance().retrieveByIdAluno(idAluno);
        for (Object o : vAlunoTurma) {
            int idTurma = ((AlunoTurma) o).getIdTurma();
            Turma turma = Control.getTurmaByIdTurma(idTurma);
            if (turma.getStatus() != 2) {
                Control.removeAlunoTurma(idAluno, idTurma);
            }
        }

    }

    //- Control Aula
    public static Aula createAula(int numero, int idTurma, Calendar data, int cargaHoraria) {
        return AulaDAO.getInstance().create(numero, idTurma, data, cargaHoraria);
    }

    public static ArrayList<Object> getAllAulasByIdTurma(int idTurma) {
        return AulaDAO.getInstance().retrieveByIdTurma(idTurma);
    }

    public static ArrayList<Object> getAllAulasDadasByIdProfessorMes(int idProfessor, int mes, int ano) {
        return AulaDAO.getInstance().retrieveAulasDadasByIdProfessorMes(idProfessor, mes, ano);
    }

    public static void updateAula(Aula aula) {
        Turma turma = Control.getTurmaByIdTurma(aula.getIdTurma());
        aula.setIdProfessor(turma.getIdProfessor());
        AulaDAO.getInstance().update(aula);
    }

    public static void cancelaAula(Aula aula) {
        aula.setNumero(0);
        //aula.setCargaHoraria(0);
        aula.setStatus(2); //Cancelada
        deleteListaPresencaDeAula(aula);
        AulaDAO.getInstance().update(aula);
        ArrayList<Object> vAulas = AulaDAO.getInstance().retrieveAulasAfterCancelledAula(aula);
        for (Object o : vAulas) {
            if (((Aula) o).getNumero() != 0) {
                ((Aula) o).setNumero(((Aula) o).getNumero() - 1);
                AulaDAO.getInstance().update((Aula) o);
            }
        }

    }

    public static Aula cancelaAulaMantemCarga(Aula aula) {
        Aula ultimaAula = AulaDAO.getInstance().retrieveUltimaAulaByIdTurma(aula.getIdTurma());
        Aula proximaAula = createNextAula(ultimaAula);
        cancelaAula(aula);
        return proximaAula;
    }

    public static Aula adicionaAulaExtra(int idTurma) {
        Aula ultimaAula = AulaDAO.getInstance().retrieveUltimaAulaByIdTurma(idTurma);
        Aula proximaAula = createNextAula(ultimaAula);
        return proximaAula;
    }

    public static boolean descancelaAula(Aula aula, int numero) {
        if (aula.getStatus() == 2) {
            aula.setNumero(numero);
            aula.setStatus(0);
            Control.createListaPresencaDeAula(aula);
            AulaDAO.getInstance().update(aula);
            ArrayList<Object> vAulas = AulaDAO.getInstance().retrieveAulasAfterCancelledAula(aula);
            for (Object o : vAulas) {
                if (((Aula) o).getNumero() != 0) {
                    ((Aula) o).setNumero(((Aula) o).getNumero() + 1);
                    AulaDAO.getInstance().update((Aula) o);
                }
            }
        } else {
            return false;
        }
        return true;
    }

    public static boolean descancelaAulaMantemCarga(Aula aula, int numero) {
        if (aula.getStatus() == 2) {
            Aula ultimaAula = AulaDAO.getInstance().retrieveUltimaAulaByIdTurma(aula.getIdTurma());
            AulaDAO.getInstance().delete(ultimaAula);
            descancelaAula(aula, numero);
        } else {
            return false;
        }
        return true;
    }

    private static Aula createNextAula(Aula aula) {
        ArrayList<Object> vTurmaHorario = getAllTurmaHorarioByIdTurma(aula.getIdTurma());
        Calendar dt = Calendar.getInstance();
        dt.setTime(aula.getData().getTime());
        dt.add(Calendar.DAY_OF_YEAR, 1);
        while (!isDateInTurmaHorario(dt, vTurmaHorario)) {
            dt.add(Calendar.DAY_OF_YEAR, 1);
        }
        Aula novaAula = createAula(aula.getNumero() + 1, aula.getIdTurma(), dt, aula.getCargaHoraria());
        createListaPresencaDeAula(novaAula);
        return novaAula;
    }

    private static boolean isDateInTurmaHorario(Calendar date, ArrayList<Object> vTurmaHorario) {
        for (Object o : vTurmaHorario) {
            if (((TurmaHorario) o).getDiaDaSemana() == date.get(Calendar.DAY_OF_WEEK)) {
                return true;
            }
        }
        return false;
    }

    public static void cadastraAulasDeTurma(Turma turma) {
        int nSemanas = numSemanasDeAula(turma);
        Calendar dtInicio = turma.getDtFim();
        dtInicio.add(Calendar.DAY_OF_YEAR, -1);
        ArrayList<Object> vTurmaHorario = getAllTurmaHorarioByIdTurma(turma.getId());
        Calendar dt;
        int nAula = 1;
        for (int s = 0; s < nSemanas; s++) {
            dt = Calendar.getInstance();
            dt.setTime(turma.getDtInicio().getTime());
            dt.add(Calendar.WEEK_OF_YEAR, s);
            for (Object o : vTurmaHorario) {
                switch (((TurmaHorario) o).getDiaDaSemana()) {
                    case 0:
                        dt.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
                        break;
                    case 1:
                        dt.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
                        break;
                    case 2:
                        dt.set(Calendar.DAY_OF_WEEK, Calendar.TUESDAY);
                        break;
                    case 3:
                        dt.set(Calendar.DAY_OF_WEEK, Calendar.WEDNESDAY);
                        break;
                    case 4:
                        dt.set(Calendar.DAY_OF_WEEK, Calendar.THURSDAY);
                        break;
                    case 5:
                        dt.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
                        break;
                    case 6:
                        dt.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
                        break;
                }
                //if(dt.after(dtInicio)){
                Aula aula = Control.createAula(nAula++, turma.getId(), dt, ((TurmaHorario) o).getCargaHoraria());
                createListaPresencaDeAula(aula);
                //}
            }
        }
    }

    //- Control Presenca
    public static ArrayList<Object> getAllPresencaByIdAula(int idAula) {
        return PresencaDAO.getInstance().retrieveByIdAula(idAula);
    }

    public static void updatePresenca(Presenca presenca) {
        PresencaDAO.getInstance().update(presenca);
    }

    private static void createListaPresencaDeAula(Aula aula) {
        ArrayList<Object> vAlunos = Control.getAllAlunosByIdTurma(aula.getIdTurma());
        for (Object o : vAlunos) {
            PresencaDAO.getInstance().create(aula.getId(), ((Pessoa) o).getId(), 0, "");
        }
    }

    private static void deleteListaPresencaDeAula(Aula aula) {
        ArrayList<Object> vAlunos = Control.getAllAlunosByIdTurma(aula.getIdTurma());
        for (Object o : vAlunos) {
            PresencaDAO.getInstance().delete(aula.getId(), ((Pessoa) o).getId());
        }
    }

    private static void createListaPresencaAlunoTurma(int idAluno, int idTurma) {
        ArrayList<Object> vAulasPendentes = AulaDAO.getInstance().retrieveAulasPendentesByIdTurma(idTurma);
        for (Object o : vAulasPendentes) {
            PresencaDAO.getInstance().create(((Aula) o).getId(), idAluno, 0, "");
        }
    }

    private static void deleteListaPresencaAlunoTurma(int idAluno, int idTurma) {
        ArrayList<Object> vAulasPendentes = AulaDAO.getInstance().retrieveAulasPendentesByIdTurma(idTurma);
        for (Object o : vAulasPendentes) {
            PresencaDAO.getInstance().delete(((Aula) o).getId(), idAluno);
        }
    }

    //- Control Avaliacao
    public static void agendaAvaliacao(Aula aula, String nome, String descricao, int peso) {
        Avaliacao avaliacao = AvaliacaoDAO.getInstance().create(aula.getId(), nome, descricao, peso);
        ArrayList<Object> vAlunos = Control.getAllAlunosByIdTurma(aula.getIdTurma());
        for (Object o : vAlunos) {
            AlunoAvaliacaoDAO.getInstance().create(((Pessoa) o).getId(), avaliacao.getId(), 0);
        }
    }

    public static Avaliacao getAvaliacaoByIdAula(int idAula) {
        return AvaliacaoDAO.getInstance().retrieveByIdAula(idAula);
    }

    public static AlunoAvaliacao getAlunoAvaliacao(int idAluno, int idAvaliacao) {
        return AlunoAvaliacaoDAO.getInstance().retrieveByIdAlunoIdAvaliacao(idAluno, idAvaliacao);
    }

    public static void addAlunoAvaliacao(int idAluno, int idAvaliacao) {
        AlunoAvaliacaoDAO.getInstance().create(idAluno, idAvaliacao, 0);
    }

    public static void removeAlunoAvaliacao(int idAluno, int idAvaliacao) {
        AlunoAvaliacaoDAO.getInstance().delete(idAluno, idAvaliacao);
    }

    public static void removeAllAlunoAvaliacaoByIdAula(int idAula) {
        Avaliacao avaliacao = Control.getAvaliacaoByIdAula(idAula);
        AlunoAvaliacaoDAO.getInstance().deleteAllByIdAvaliacao(avaliacao.getId());
    }

    public static void updateAlunoAvaliacao(AlunoAvaliacao alunoAvaliacao) {
        AlunoAvaliacaoDAO.getInstance().update(alunoAvaliacao);
    }

    //- Control Hora Aula
    public static void createHoraAulaTurma(int idTurma, int idProfessor, double valor) {
        HoraAulaDAO.getInstance().create(idTurma, idProfessor, valor);
    }

    public static HoraAula getHoraAulaByIdTurmaIdProfessor(int idTurma, int idProfessor) {
        return HoraAulaDAO.getInstance().retrieveByIdTurmaIdProfessor(idTurma, idProfessor);
    }

    public static void updateHoraAula(HoraAula horaAula) {
        HoraAulaDAO.getInstance().update(horaAula);
    }
}// Control

