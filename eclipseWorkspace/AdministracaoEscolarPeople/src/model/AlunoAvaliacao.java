package model;

/**
 * @author Prof. Dr. Plinio Vilela - vilela@ydoo.com.br
 */
public class AlunoAvaliacao {
	private int idAluno;
	private int idAvaliacao;
	private double nota;
	public AlunoAvaliacao(int idAluno, int idAvaliacao, double nota) {
		super();
		this.idAluno = idAluno;
		this.idAvaliacao = idAvaliacao;
		this.nota = nota;
	}
	public int getIdAluno() {
		return idAluno;
	}
	public void setIdAluno(int idAluno) {
		this.idAluno = idAluno;
	}
	public int getIdAvaliacao() {
		return idAvaliacao;
	}
	public void setIdAvaliacao(int idAvaliacao) {
		this.idAvaliacao = idAvaliacao;
	}
	public double getNota() {
		return nota;
	}
	public void setNota(double nota) {
		this.nota = nota;
	}	
}// AlunoAvaliacao
