package model;

import java.util.Calendar;

/**
 *
 * @author prvilela
 */
public class Sistema {
    private int id;
    private int versao;
    private String descricao;
    private Calendar data;

    public Sistema(int id, int versao, String descricao, Calendar data) {
        this.id = id;
        this.versao = versao;
        this.descricao = descricao;
        this.data = data;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVersao() {
        return versao;
    }

    public void setVersao(int versao) {
        this.versao = versao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Calendar getData() {
        return data;
    }

    public void setData(Calendar data) {
        this.data = data;
    }
    
}
