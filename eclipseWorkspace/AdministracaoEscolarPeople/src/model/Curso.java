package model;

/**
 * @author Prof. Dr. Plinio Vilela - vilela@ydoo.com.br
 */
public class Curso {
	private int id;
	private int idTipo;
	private String nome;
	public Curso(int id, int idTipo, String nome) {
		super();
		this.id = id;
		this.idTipo = idTipo;
		this.nome = nome;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIdTipo() {
		return idTipo;
	}
	public void setIdTipo(int idTipo) {
		this.idTipo = idTipo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}	
	public String toString(){
		return nome;
	}
}
