package model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Prof. Dr. Plinio Vilela - vilela@ydoo.com.br
 */
public class TurmaHorarioDAO {
	private static TurmaHorarioDAO instance;
	private GenericCONN myCONN;

	private TurmaHorarioDAO() {
		try {
			Class<?> daoClass = Class.forName("model." + GenericCONN.DAOName);
			myCONN = (GenericCONN) daoClass.newInstance();
		} catch (Exception e) {
			System.out.println("Could not find DAO class: " + "model." + GenericCONN.DAOName);
		}
		myCONN.getConnection();
	}

	public static TurmaHorarioDAO getInstance() {
		if (instance == null) {
			instance = new TurmaHorarioDAO();
		}
		return instance;
	}

	// CRUD
	public void create(int idTurma, int diaDaSemana, int horario, int cargaHoraria) {
		PreparedStatement stmt;
		try {
			stmt = myCONN.getConnection().prepareStatement("INSERT INTO TurmaHorario (idTurma, diaDaSemana, horario, cargaHoraria) VALUES (?,?,?,?)");
			stmt.setInt(1, idTurma);
			stmt.setInt(2, diaDaSemana);
			stmt.setInt(3, horario);
			stmt.setInt(4, cargaHoraria);
			myCONN.executeUpdate(stmt);
		} catch (SQLException ex) {
			Logger.getLogger(TurmaHorarioDAO.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
    private TurmaHorario buildObject(ResultSet rs){
    	TurmaHorario turmaHorario = null;
    	try {
			turmaHorario = new TurmaHorario(rs.getInt("id"), rs.getInt("idTurma"), rs.getInt("diaDaSemana"), rs.getInt("horario"), rs.getInt("cargaHoraria"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
    	return turmaHorario;
    }

	public ArrayList<Object> retrieveByIdTurma(int idTurma) {
		PreparedStatement stmt;
		ArrayList<Object> turmaHorarios = new ArrayList<Object>();
		ResultSet rs;
		try {
			stmt = myCONN.getConnection().prepareStatement("SELECT * FROM TurmaHorario WHERE idTurma=? ORDER BY id");
			stmt.setInt(1, idTurma);
			rs = myCONN.getResultSet(stmt);
			while (rs.next()) {
				TurmaHorario t = buildObject(rs);
				turmaHorarios.add(t);
			}
			rs.close();
		} catch (SQLException ex) {
			Logger.getLogger(TurmaHorarioDAO.class.getName()).log(Level.SEVERE, null, ex);
		}
		return turmaHorarios;
	}

	public TurmaHorario retrieveById(int id) {
		PreparedStatement stmt;
		TurmaHorario turmaHorario = null;
		ResultSet rs;
		try {
			stmt = myCONN.getConnection().prepareStatement("SELECT * FROM TurmaHorario WHERE id=?");
			stmt.setInt(1, id);
			rs = myCONN.getResultSet(stmt);
			if (rs.next()) {
				turmaHorario = buildObject(rs);
			}
			rs.close();
		} catch (SQLException ex) {
			Logger.getLogger(TurmaHorarioDAO.class.getName()).log(Level.SEVERE, null, ex);
		}
		return turmaHorario;
	}
  
	public TurmaHorario retrieveLastId() {
		int id = myCONN.lastId("TurmaHorario", "id");
		return retrieveById(id);
	}

	public boolean update(TurmaHorario turmaHorario) {
		PreparedStatement stmt;
		try {
			stmt = myCONN.getConnection().prepareStatement("UPDATE TurmaHorario SET diaDaSemana=?, horario=?, cargaHoraria=? WHERE id = ?");
			stmt.setInt(1, turmaHorario.getDiaDaSemana());
			stmt.setInt(2, turmaHorario.getHorario());
			stmt.setInt(3, turmaHorario.getCargaHoraria());
			stmt.setInt(4, turmaHorario.getId());
			int update = myCONN.executeUpdate(stmt);
			if (update == 1) {
				return true;
			}
		} catch (SQLException ex) {
			Logger.getLogger(TurmaHorarioDAO.class.getName()).log(Level.SEVERE, null, ex);
		}
		return false;
	}

	public void delete(TurmaHorario turmaHorario) {
		PreparedStatement stmt;
		try {
			stmt = myCONN.getConnection().prepareStatement("DELETE FROM TurmaHorario WHERE id = ?");
			stmt.setInt(1, turmaHorario.getId());
			myCONN.executeUpdate(stmt);
		} catch (SQLException ex) {
			Logger.getLogger(TurmaHorarioDAO.class.getName()).log(Level.SEVERE, null, ex);
		}
	}  
	
	public void deleteAllByIdTurma(int idTurma){
		PreparedStatement stmt;
		try {
			stmt = myCONN.getConnection().prepareStatement("DELETE FROM TurmaHorario WHERE idTurma = ?");
			stmt.setInt(1, idTurma);
			myCONN.executeUpdate(stmt);
		} catch (SQLException ex) {
			Logger.getLogger(TurmaHorarioDAO.class.getName()).log(Level.SEVERE, null, ex);
		}		
	}
	
}// TurmaHorarioDAO
