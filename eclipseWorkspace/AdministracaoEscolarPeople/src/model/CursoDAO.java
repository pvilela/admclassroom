package model;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * @author Prof. Dr. Plinio Vilela - vilela@ydoo.com.br
 */
public class CursoDAO {
    private static CursoDAO instance;
    private GenericCONN myCONN;

    private CursoDAO() {
        try {
            Class<?> daoClass = Class.forName("model." + GenericCONN.DAOName);
            myCONN = (GenericCONN) daoClass.newInstance();
        } catch (Exception e) {
            System.out.println("Could not find DAO class: " + "model." + GenericCONN.DAOName);
        }
        myCONN.getConnection();
    }

    public static CursoDAO getInstance() {
        if (instance == null) {
            instance = new CursoDAO();
        }
        return instance;
    }

    // CRUD
    public void create(int idTipoCurso, String nome) {
        PreparedStatement stmt;
        try {
            stmt = myCONN.getConnection().prepareStatement("INSERT INTO Curso (idTipoCurso, nome) VALUES (?,?)");
            stmt.setInt(1, idTipoCurso);
            stmt.setString(2, nome);
            myCONN.executeUpdate(stmt);
        } catch (SQLException ex) {
            Logger.getLogger(CursoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private Curso buildObject(ResultSet rs){
    	Curso curso = null;
    	try {
			curso = new Curso(rs.getInt("id"), rs.getInt("idTipoCurso"), rs.getString("nome"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
    	return curso;
    }
    
    public ArrayList<Object> retrieveAll() {
        PreparedStatement stmt;
        ArrayList<Object> cursos = new ArrayList<Object>();
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Curso ORDER BY nome");
            rs = myCONN.getResultSet(stmt);
            while (rs.next()) {
                cursos.add(buildObject(rs));
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(CursoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cursos;
    }

    public ArrayList<Curso> retrieveLike(String nome) {
        PreparedStatement stmt;
        ArrayList<Curso> cursos = new ArrayList<Curso>();
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Curso WHERE nome LIKE '%"+nome+"%' ORDER BY nome");
            rs = myCONN.getResultSet(stmt);
            while (rs.next()) {
                cursos.add(buildObject(rs));
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(CursoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cursos;
    }

    public Curso retrieveById(int id) {
        PreparedStatement stmt;
        Curso curso = null;
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Curso where id=?");
            stmt.setInt(1, id);
            rs = myCONN.getResultSet(stmt);
            if (rs.next()) {
                curso = buildObject(rs);
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(CursoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return curso;
    }
    
    public ArrayList<Object> retrieveByTipoCurso(int idTipoCurso) {
        PreparedStatement stmt;
        ArrayList<Object> cursos = new ArrayList<Object>();
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Curso where idTipoCurso=?");
            stmt.setInt(1, idTipoCurso);
            rs = myCONN.getResultSet(stmt);
            while (rs.next()) {
                cursos.add(buildObject(rs));
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(CursoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cursos;
    }    

    public Curso retrieveLastId() {
        int id = myCONN.lastId("Curso", "id");
        return retrieveById(id);
    }

    public boolean update(Curso curso) {
        PreparedStatement stmt;
        try {
            stmt = myCONN.getConnection().prepareStatement("UPDATE Curso SET nome=?, idTipoCurso=? WHERE id = ?");
            stmt.setString(1, curso.getNome());
            stmt.setInt(2, curso.getIdTipo());
            stmt.setInt(3, curso.getId());
            int update = myCONN.executeUpdate(stmt);
            if (update == 1) {
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(CursoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public void delete(Curso curso) {
        PreparedStatement stmt;
        try {
            stmt = myCONN.getConnection().prepareStatement("DELETE FROM Curso WHERE id = ?");
            stmt.setInt(1, curso.getId());
            myCONN.executeUpdate(stmt);
            CursoModuloDAO.getInstance().deleteAllByIdCurso(curso.getId());
        } catch (SQLException ex) {
            Logger.getLogger(CursoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}//CursoDAO
