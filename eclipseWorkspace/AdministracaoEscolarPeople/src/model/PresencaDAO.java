package model;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Prof. Dr. Plinio Vilela - vilela@ydoo.com.br
 */
public class PresencaDAO {
	private static PresencaDAO instance;
	private GenericCONN myCONN;

	private PresencaDAO() {
		try {
			Class<?> daoClass = Class.forName("model." + GenericCONN.DAOName);
			myCONN = (GenericCONN) daoClass.newInstance();
		} catch (Exception e) {
			System.out.println("Could not find DAO class: " + "model." + GenericCONN.DAOName);
		}
		myCONN.getConnection();
	}

	public static PresencaDAO getInstance() {
		if (instance == null) {
			instance = new PresencaDAO();
		}
		return instance;
	}

	// CRUD
	public void create(int idAula, int idAluno, int presenca, String observacao) {
		PreparedStatement stmt;
		try {
			stmt = myCONN.getConnection().prepareStatement("INSERT INTO Presenca (idAula, idAluno, presenca, observacao) VALUES (?,?,?,?)");
			stmt.setInt(1, idAula);
			stmt.setInt(2, idAluno);
			stmt.setInt(3, presenca);
			stmt.setString(4, observacao);
			myCONN.executeUpdate(stmt);
		} catch (SQLException ex) {
			Logger.getLogger(PresencaDAO.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
    private Presenca buildObject(ResultSet rs){
    	Presenca presenca = null;
    	try {
			presenca = new Presenca(rs.getInt("id"), rs.getInt("idAula"), rs.getInt("idAluno"), rs.getInt("presenca"), rs.getString("observacao"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
    	return presenca;
    }

	public ArrayList<Object> retrieveByIdAula(int idAula) {
		PreparedStatement stmt;
		ArrayList<Object> presencas = new ArrayList<Object>();
		ResultSet rs;
		try {
			stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Presenca WHERE idAula=? ORDER BY id");
			stmt.setInt(1, idAula);
			rs = myCONN.getResultSet(stmt);
			while (rs.next()) {
				Presenca t = buildObject(rs);
				presencas.add(t);
			}
			rs.close();
		} catch (SQLException ex) {
			Logger.getLogger(PresencaDAO.class.getName()).log(Level.SEVERE, null, ex);
		}
		return presencas;
	}

	public Presenca retrieveById(int id) {
		PreparedStatement stmt;
		Presenca presenca = null;
		ResultSet rs;
		try {
			stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Presenca WHERE id=?");
			stmt.setInt(1, id);
			rs = myCONN.getResultSet(stmt);
			if (rs.next()) {
				presenca = buildObject(rs);
			}
			rs.close();
		} catch (SQLException ex) {
			Logger.getLogger(PresencaDAO.class.getName()).log(Level.SEVERE, null, ex);
		}
		return presenca;
	}
  
	public Presenca retrieveLastId() {
		int id = myCONN.lastId("Presenca", "id");
		return retrieveById(id);
	}

	
	public boolean update(Presenca presenca) {
		PreparedStatement stmt;
		try {
			stmt = myCONN.getConnection().prepareStatement("UPDATE Presenca SET presenca=?, observacao=? WHERE id = ?");
			stmt.setInt(1, presenca.getPresenca());
			stmt.setString(2, presenca.getObservacao());
			stmt.setInt(3, presenca.getId());
			int update = myCONN.executeUpdate(stmt);
			if (update == 1) {
				return true;
			}
		} catch (SQLException ex) {
			Logger.getLogger(PresencaDAO.class.getName()).log(Level.SEVERE, null, ex);
		}
		return false;
	}
	

	public void delete(Presenca presenca) {
		PreparedStatement stmt;
		try {
			stmt = myCONN.getConnection().prepareStatement("DELETE FROM Presenca WHERE id = ?");
			stmt.setInt(1, presenca.getId());
			myCONN.executeUpdate(stmt);
		} catch (SQLException ex) {
			Logger.getLogger(PresencaDAO.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	
	public void delete(int idAula, int idAluno) {
		PreparedStatement stmt;
		try {
			stmt = myCONN.getConnection().prepareStatement("DELETE FROM Presenca WHERE idAula = ? AND idAluno = ?");
			stmt.setInt(1, idAula);
			stmt.setInt(2, idAluno);
			myCONN.executeUpdate(stmt);
		} catch (SQLException ex) {
			Logger.getLogger(PresencaDAO.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
}// PresencaDAO
