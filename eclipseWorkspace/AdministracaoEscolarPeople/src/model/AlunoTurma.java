package model;

import java.util.Calendar;

/**
 * @author Prof. Dr. Plinio Vilela - vilela@ydoo.com.br
 */
public class AlunoTurma {
	private int idAluno;
	private int idTurma;
	private Calendar dtMatricula;
	private Calendar dtCancelamento;
	private int status;
	/*
	 * 0 - regular
	 * 1 - inadimplente
	 * 2 - cancelado
	 * 3 - concluido
	 */
	public AlunoTurma(int idAluno, int idTurma, Calendar dtMatricula,
			Calendar dtCancelamento, int status) {
		super();
		this.idAluno = idAluno;
		this.idTurma = idTurma;
		this.dtMatricula = dtMatricula;
		this.dtCancelamento = dtCancelamento;
		this.status = status;
	}
	public int getIdAluno() {
		return idAluno;
	}
	public void setIdAluno(int idAluno) {
		this.idAluno = idAluno;
	}
	public int getIdTurma() {
		return idTurma;
	}
	public void setIdTurma(int idTurma) {
		this.idTurma = idTurma;
	}
	public Calendar getDtMatricula() {
		return dtMatricula;
	}
	public void setDtMatricula(Calendar dtMatricula) {
		this.dtMatricula = dtMatricula;
	}
	public Calendar getDtCancelamento() {
		return dtCancelamento;
	}
	public void setDtCancelamento(Calendar dtCancelamento) {
		this.dtCancelamento = dtCancelamento;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}		
}// AlunoTurma
