package model;

/**
 * @author Prof. Dr. Plinio Vilela - vilela@ydoo.com.br
 */
public class TurmaHorario {
	private int id;
	private int idTurma;
	private int diaDaSemana;
	private int horario;
	private int cargaHoraria;
	public TurmaHorario(int id, int idTurma, int diaDaSemana, int horario,
			int cargaHoraria) {
		super();
		this.id = id;
		this.idTurma = idTurma;
		this.diaDaSemana = diaDaSemana;
		this.horario = horario;
		this.cargaHoraria = cargaHoraria;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIdTurma() {
		return idTurma;
	}
	public void setIdTurma(int idTurma) {
		this.idTurma = idTurma;
	}
	public int getDiaDaSemana() {
		return diaDaSemana;
	}
	public void setDiaDaSemana(int diaDaSemana) {
		this.diaDaSemana = diaDaSemana;
	}
	public int getHorario() {
		return horario;
	}
	public void setHorario(int horario) {
		this.horario = horario;
	}
	public int getCargaHoraria() {
		return cargaHoraria;
	}
	public void setCargaHoraria(int cargaHoraria) {
		this.cargaHoraria = cargaHoraria;
	}
	public String getStrHorario(){
		return new String((this.getHorario()/2)+7+"")+((this.getHorario()%2)==0?("h"):(":30h"));
	}
}
