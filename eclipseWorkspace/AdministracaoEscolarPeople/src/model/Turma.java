package model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * @author Prof. Dr. Plinio Vilela - vilela@ydoo.com.br
 */
public class Turma {
	private int id;
	private String descricao;
	private int idCurso;
	private int idProfessor;
	private Calendar dtInicio;
	private Calendar dtFim;
	private int status;
	/*
	 * 0 - nao iniciou
	 * 1 - em andamento
	 * 2 - encerrado
	 */        
	private DateFormat dateFormat;
	private boolean aulasGeradas;
	private String observacoes;
	public Turma(int id, String descricao, int idCurso, int idProfessor, Calendar dtInicio,
			Calendar dtFim, int status, String observacoes, boolean aulasGeradas) {
		super();
		this.id = id;
		this.descricao = descricao;
		this.idCurso = idCurso;
		this.idProfessor = idProfessor;
		this.dtInicio = dtInicio;
		this.dtFim = dtFim;
		this.status = status;
		this.observacoes = observacoes;
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		this.aulasGeradas = aulasGeradas;
	}
	public Turma(int id, int idCurso, int status) {
		super();
		this.id = id;
		this.idCurso = idCurso;
		this.status = status;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}	
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public int getIdCurso() {
		return idCurso;
	}
	public void setIdCurso(int idCurso) {
		this.idCurso = idCurso;
	}
	public int getIdProfessor() {
		return idProfessor;
	}
	public void setIdProfessor(int idProfessor) {
		this.idProfessor = idProfessor;
	}
	public Calendar getDtInicio() {
		return dtInicio;
	}
	public void setDtInicio(Calendar dtInicio) {
		this.dtInicio = dtInicio;
	}
	public Calendar getDtFim() {
		return dtFim;
	}
	public void setDtFim(Calendar dtFim) {
		this.dtFim = dtFim;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getObservacoes() {
		return observacoes;
	}
	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}		
	public boolean isAulasGeradas() {
		return aulasGeradas;
	}
	public void setAulasGeradas(boolean aulasGeradas) {
		this.aulasGeradas = aulasGeradas;
	}
	public String toString(){
		String strStatus;
		switch(status){
		case 0:
			strStatus = "Nao iniciou.";
			break;
		case 1:
			strStatus = "Em andamento.";
			break;
		case 2:
			strStatus = "Encerrou.";
			break;
		default:
			strStatus = "";
		}
		return ((Curso)CursoDAO.getInstance().retrieveById(idCurso)).getNome()+" : "+descricao+" : "+dateFormat.format(dtInicio.getTime())+" - "+strStatus;
	}
}
