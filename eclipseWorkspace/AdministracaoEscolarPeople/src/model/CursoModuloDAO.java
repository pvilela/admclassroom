package model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Prof. Dr. Plinio Vilela - vilela@ydoo.com.br
 */
public class CursoModuloDAO {
	private static CursoModuloDAO instance;
	private GenericCONN myCONN;

	private CursoModuloDAO() {
		try {
			Class<?> daoClass = Class.forName("model." + GenericCONN.DAOName);
			myCONN = (GenericCONN) daoClass.newInstance();
		} catch (Exception e) {
			System.out.println("Could not find DAO class: " + "model." + GenericCONN.DAOName);
		}
		myCONN.getConnection();
	}

	public static CursoModuloDAO getInstance() {
		if (instance == null) {
			instance = new CursoModuloDAO();
		}
		return instance;
	}

	// CRUD
	public void create(int idCurso, int idModulo) {
		PreparedStatement stmt;
		//if(!isCursoModuloCadastrado(idCurso,idModulo)){
			try {
				stmt = myCONN.getConnection().prepareStatement("INSERT INTO CursoModulo (idCurso, idModulo) VALUES (?,?)");
				stmt.setInt(1, idCurso);
				stmt.setInt(2, idModulo);
				myCONN.executeUpdate(stmt);
			} catch (SQLException ex) {
				//Logger.getLogger(CursoModuloDAO.class.getName()).log(Level.SEVERE, null, ex);
			}
		//}
	}

	private Modulo buildObject(ResultSet rs){
		Modulo modulo = null;
		try {
			modulo = new Modulo(rs.getInt("id"), rs.getString("nome"), rs.getInt("cargaHoraria"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return modulo;
	}

	public ArrayList<Object> retrieveAllModuloByIdCurso(int idCurso) {
		PreparedStatement stmt;
		ArrayList<Object> modulos = new ArrayList<Object>();
		ResultSet rs;
		try {
			stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Modulo INNER JOIN CursoModulo ON Modulo.id = CursoModulo.idModulo WHERE CursoModulo.idCurso=? ORDER BY idModulo");
			stmt.setInt(1, idCurso);
			rs = myCONN.getResultSet(stmt);
			while (rs.next()) {
				modulos.add(buildObject(rs));
			}
			rs.close();
		} catch (SQLException ex) {
			Logger.getLogger(CursoModuloDAO.class.getName()).log(Level.SEVERE, null, ex);
		}
		return modulos;
	}

	public boolean isModuloBeingUsed(int idModulo) {
		PreparedStatement stmt;
		ResultSet rs;
		try {
			stmt = myCONN.getConnection().prepareStatement("SELECT * FROM CursoModulo WHERE idModulo=?");
			stmt.setInt(1, idModulo);            
			rs = myCONN.getResultSet(stmt);
			if (rs.next()) {
				return true;
			}
			rs.close();
		} catch (SQLException ex) {
			Logger.getLogger(CursoModuloDAO.class.getName()).log(Level.SEVERE, null, ex);
		}
		return false;
	}

	public void delete(int idCurso, int idModulo) {
		PreparedStatement stmt;
		try {
			stmt = myCONN.getConnection().prepareStatement("DELETE FROM CursoModulo WHERE idCurso=? AND idModulo=?");
			stmt.setInt(1, idCurso);
			stmt.setInt(2, idModulo);            
			myCONN.executeUpdate(stmt);
		} catch (SQLException ex) {
			Logger.getLogger(CursoModuloDAO.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	public void deleteAllByIdCurso(int idCurso) {
		PreparedStatement stmt;
		try {
			stmt = myCONN.getConnection().prepareStatement("DELETE FROM CursoModulo WHERE idCurso=?");
			stmt.setInt(1, idCurso);
			myCONN.executeUpdate(stmt);
		} catch (SQLException ex) {
			Logger.getLogger(CursoModuloDAO.class.getName()).log(Level.SEVERE, null, ex);
		}
	}	
	
}
