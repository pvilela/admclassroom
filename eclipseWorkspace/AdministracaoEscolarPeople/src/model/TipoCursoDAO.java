package model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TipoCursoDAO {
    private static TipoCursoDAO instance;
    private GenericCONN myCONN;

    private TipoCursoDAO() {
        try {
            Class<?> daoClass = Class.forName("model." + GenericCONN.DAOName);
            myCONN = (GenericCONN) daoClass.newInstance();
        } catch (Exception e) {
            System.out.println("Could not find DAO class: " + "model." + GenericCONN.DAOName);
        }
        myCONN.getConnection();
    }

    public static TipoCursoDAO getInstance() {
        if (instance == null) {
            instance = new TipoCursoDAO();
        }
        return instance;
    }

    // CRUD
    public void create(String nome) {
        PreparedStatement stmt;
        try {
            stmt = myCONN.getConnection().prepareStatement("INSERT INTO TipoCurso (nome) VALUES (?)");
            stmt.setString(1, nome);
            myCONN.executeUpdate(stmt);
        } catch (SQLException ex) {
            Logger.getLogger(TipoCursoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private TipoCurso buildObject(ResultSet rs){
    	TipoCurso tipoCurso = null;
    	try {
			tipoCurso = new TipoCurso(rs.getInt("id"), rs.getString("nome"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
    	return tipoCurso;
    }

    public ArrayList<Object> retrieveAll() {
        PreparedStatement stmt;
        ArrayList<Object> tipoCursos = new ArrayList<Object>();
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM TipoCurso ORDER BY id");
            rs = myCONN.getResultSet(stmt);
            while (rs.next()) {
                tipoCursos.add(buildObject(rs));
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(TipoCursoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tipoCursos;
    }

    public ArrayList<TipoCurso> retrieveLike(String nome) {
        PreparedStatement stmt;
        ArrayList<TipoCurso> tipoCursos = new ArrayList<TipoCurso>();
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM TipoCurso WHERE nome LIKE '%"+nome+"%' ORDER BY nome");
            rs = myCONN.getResultSet(stmt);
            while (rs.next()) {
                tipoCursos.add(buildObject(rs));
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(TipoCursoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tipoCursos;
    }

    public TipoCurso retrieveById(int id) {
        PreparedStatement stmt;
        TipoCurso tipoTipoCurso = null;
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM TipoCurso where id=?");
            stmt.setInt(1, id);
            rs = myCONN.getResultSet(stmt);
            if (rs.next()) {
                tipoTipoCurso = buildObject(rs);
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(TipoCursoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tipoTipoCurso;
    } 

    public TipoCurso retrieveLastId() {
        int id = myCONN.lastId("TipoCurso", "id");
        return retrieveById(id);
    }

    public boolean update(TipoCurso tipoCurso) {
        PreparedStatement stmt;
        try {
            stmt = myCONN.getConnection().prepareStatement("UPDATE TipoCurso SET nome=? WHERE id = ?");
            stmt.setString(1, tipoCurso.getNome());
            stmt.setInt(2, tipoCurso.getId());
            int update = myCONN.executeUpdate(stmt);
            if (update == 1) {
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(TipoCursoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public void delete(TipoCurso tipoCurso) {
        PreparedStatement stmt;
        try {
            stmt = myCONN.getConnection().prepareStatement("DELETE FROM TipoCurso WHERE id = ?");
            stmt.setInt(1, tipoCurso.getId());
            myCONN.executeUpdate(stmt);
        } catch (SQLException ex) {
            Logger.getLogger(TipoCursoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
