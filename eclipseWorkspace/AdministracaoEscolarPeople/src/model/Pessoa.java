package model;

import control.Control;
import java.awt.Image;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

/**
 * @author Prof. Dr. Plinio Vilela - vilela@ydoo.com.br
 */
public class Pessoa {

    private int id;
    private String nome;
    private String telefone;
    private String email;
    private Calendar dataNascimento;
    private int tipo;
    private int idResponsavel;
    private int status;
    /*
     * Tipos definidos:
     * 0 - funcionario
     * 1 - professor
     * 2 - diretor
     * 3 - admin
     * 4 - aluno
     * 
     * Status
     * 0 - inativo
     * 1 - ativo (default)
     */
    private InputStream foto;
    public long size;
    private ImageIcon myImageIcon;

    public Pessoa(int id, String nome, String telefone, String email,
            Calendar dataNascimento, int tipo, int idResponsavel, int status) {
        super();
        this.id = id;
        this.nome = nome;
        this.telefone = telefone;
        this.email = email;
        this.dataNascimento = dataNascimento;
        this.tipo = tipo;
        this.idResponsavel = idResponsavel;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Calendar getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Calendar dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public int getIdResponsavel() {
        return idResponsavel;
    }

    public void setIdResponsavel(int idResponsavel) {
        this.idResponsavel = idResponsavel;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String toString() {
        return nome;
    }
    public InputStream getFoto() {
        return foto;
    }

    public void setFoto(InputStream foto, long size) {
        this.foto = foto;
        this.size = size;
    }

    public void setImageIcon(String path) throws FileNotFoundException, IOException {
        File image = new File(path);
        long size = image.length();
        FileInputStream fis = new FileInputStream(image);

        this.setFoto(fis, size);
        Control.updatePessoa(this);
        Pessoa p = Control.getPessoaById(this.getId());
        this.setFoto(p.getFoto(), p.size);
    }

    public ImageIcon getImageIcon() throws IOException {
        Image img = ImageIO.read(new BufferedInputStream(this.getFoto()));

        ImageIcon icon = new ImageIcon(img);
        Image newImg = icon.getImage();
        Image resizedImg = newImg.getScaledInstance(170, 230, Image.SCALE_SMOOTH);

        myImageIcon = new ImageIcon(resizedImg);
        return myImageIcon;
    }
    
}
