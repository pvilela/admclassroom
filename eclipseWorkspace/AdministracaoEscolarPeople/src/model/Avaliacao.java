package model;

/**
 * @author Prof. Dr. Plinio Vilela - vilela@ydoo.com.br
 */
public class Avaliacao {
	private int id;
	private int idAula;
	private String nome;
	private String descricao;
	private int peso;
	public Avaliacao(int id, int idAula, String nome,
			String descricao, int peso) {
		super();
		this.id = id;
		this.idAula = idAula;
		this.nome = nome;
		this.descricao = descricao;
		this.peso = peso;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIdAula() {
		return idAula;
	}
	public void setIdAula(int idAula) {
		this.idAula = idAula;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public int getPeso() {
		return peso;
	}
	public void setPeso(int peso) {
		this.peso = peso;
	}
}// Avaliacao
