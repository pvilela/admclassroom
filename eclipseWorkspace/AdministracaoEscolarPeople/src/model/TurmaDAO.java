package model;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * @author Prof. Dr. Plinio Vilela - vilela@ydoo.com.br
 */
public class TurmaDAO {
	private static TurmaDAO instance;
	private GenericCONN myCONN;

	private TurmaDAO() {
		try {
			Class<?> daoClass = Class.forName("model." + GenericCONN.DAOName);
			myCONN = (GenericCONN) daoClass.newInstance();
		} catch (Exception e) {
			System.out.println("Could not find DAO class: " + "model." + GenericCONN.DAOName);
		}
		myCONN.getConnection();
	}

	public static TurmaDAO getInstance() {
		if (instance == null) {
			instance = new TurmaDAO();
		}
		return instance;
	}

	// CRUD
	public void create(int idCurso, int status) {
		PreparedStatement stmt;
		try {
			stmt = myCONN.getConnection().prepareStatement("INSERT INTO Turma (idCurso, status) VALUES (?,?)");
			stmt.setInt(1, idCurso);
			stmt.setInt(2, status);
			myCONN.executeUpdate(stmt);
		} catch (SQLException ex) {
			Logger.getLogger(TurmaDAO.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	private Turma buildObject(ResultSet rs){
		Calendar dtInicio, dtFim;
		dtInicio = Calendar.getInstance();
		dtFim = Calendar.getInstance();
		Turma turma = null;
		try {
			Date dateInicio = rs.getDate("dtInicio");
			Date dateFim = rs.getDate("dtFim");
			if(dateInicio!=null)
				dtInicio.setTime(dateInicio);
			if(dateFim!=null)
				dtFim.setTime(dateFim);
			turma = new Turma(rs.getInt("id"), rs.getString("descricao"), rs.getInt("idCurso"), rs.getInt("idProfessor"), dtInicio, dtFim, rs.getInt("status"), rs.getString("observacoes"),((rs.getInt("aulasGeradas")==1)?true:false));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return turma;
	}

	public ArrayList<Object> retrieveAll() {
		PreparedStatement stmt;
		ArrayList<Object> turmas = new ArrayList<Object>();
		ResultSet rs;
		try {
			stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Turma ORDER BY dtInicio");
			rs = myCONN.getResultSet(stmt);
			while (rs.next()) {
				Turma t = buildObject(rs);
				turmas.add(t);
			}
			rs.close();
		} catch (SQLException ex) {
			Logger.getLogger(TurmaDAO.class.getName()).log(Level.SEVERE, null, ex);
		}
		return turmas;
	}

	public Turma retrieveById(int id) {
		PreparedStatement stmt;
		Turma turma = null;
		ResultSet rs;
		try {
			stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Turma WHERE id=?");
			stmt.setInt(1, id);
			rs = myCONN.getResultSet(stmt);
			if (rs.next()) {
				turma = buildObject(rs);
			}
			rs.close();
		} catch (SQLException ex) {
			Logger.getLogger(TurmaDAO.class.getName()).log(Level.SEVERE, null, ex);
		}
		return turma;
	}
       

        public ArrayList<Object> retrieveByIdAluno(int idAluno) {
		PreparedStatement stmt;
		ArrayList<Object> turmas = new ArrayList<Object>();
		ResultSet rs;
		try {
			stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Turma T, Pessoa P, AlunoTurma AT WHERE P.id = AT.idAluno AND AT.idTurma = T.id AND P.id=? ORDER BY T.dtInicio");
			stmt.setInt(1, idAluno);            
			rs = myCONN.getResultSet(stmt);
			while (rs.next()) {
				Turma t = buildObject(rs);
				turmas.add(t);
			}
			rs.close();
		} catch (SQLException ex) {
			Logger.getLogger(TurmaDAO.class.getName()).log(Level.SEVERE, null, ex);
		}
		return turmas;
	}  
        
        
	public ArrayList<Turma> retrieveByCurso(int idCurso) {
		PreparedStatement stmt;
		ArrayList<Turma> turmas = new ArrayList<Turma>();
		ResultSet rs;
		try {
			stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Turma WHERE idCurso=? ORDER BY dtInicio");
			stmt.setInt(1, idCurso);            
			rs = myCONN.getResultSet(stmt);
			while (rs.next()) {
				Turma t = buildObject(rs);
				turmas.add(t);
			}
			rs.close();
		} catch (SQLException ex) {
			Logger.getLogger(TurmaDAO.class.getName()).log(Level.SEVERE, null, ex);
		}
		return turmas;
	}    

	public ArrayList<Turma> retrieveByStatus(int status) {
		PreparedStatement stmt;
		ArrayList<Turma> turmas = new ArrayList<Turma>();
		ResultSet rs;
		try {
			stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Turma WHERE status=? ORDER BY dtInicio");
			stmt.setInt(1, status);            
			rs = myCONN.getResultSet(stmt);
			while (rs.next()) {
				Turma t = buildObject(rs);
				turmas.add(t);
			}
			rs.close();
		} catch (SQLException ex) {
			Logger.getLogger(TurmaDAO.class.getName()).log(Level.SEVERE, null, ex);
		}
		return turmas;
	}      

	public ArrayList<Object> retrieveByStatusBusca(int statusBusca) {
		PreparedStatement stmt;
		ArrayList<Object> turmas = new ArrayList<Object>();
		ResultSet rs;
		try {
			switch(statusBusca){
			case 1:
				stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Turma WHERE status=2 ORDER BY dtInicio");
				break;
			case 10:
				stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Turma WHERE status=1 ORDER BY dtInicio");
				break;
			case 11:
				stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Turma WHERE status=1 OR status=2 ORDER BY dtInicio");
				break;
			case 100:
				stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Turma WHERE status=0 ORDER BY dtInicio");
				break;
			case 101:
				stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Turma WHERE status=0 OR status=2 ORDER BY dtInicio");
				break;
			case 110:
				stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Turma WHERE status=0 OR status=1 ORDER BY dtInicio");
				break;
			default:
				stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Turma ORDER BY dtInicio");
			}
			rs = myCONN.getResultSet(stmt);
			while (rs.next()) {
				Turma t = buildObject(rs);
				turmas.add(t);
			}
			rs.close();
		} catch (SQLException ex) {
			Logger.getLogger(TurmaDAO.class.getName()).log(Level.SEVERE, null, ex);
		}
		return turmas;
	}

	public ArrayList<Object> retrieveByStatusBuscaByIdProfessor(int statusBusca, int idProfessor) {
		PreparedStatement stmt;
		ArrayList<Object> turmas = new ArrayList<Object>();
		ResultSet rs;
		try {
			switch(statusBusca){
			case 1:
				stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Turma WHERE status=2 AND idProfessor=? ORDER BY dtInicio");
				break;
			case 10:
				stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Turma WHERE status=1 AND idProfessor=? ORDER BY dtInicio");
				break;
			case 11:
				stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Turma WHERE (status=1 OR status=2) AND idProfessor=? ORDER BY dtInicio");
				break;
			case 100:
				stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Turma WHERE status=0 AND idProfessor=? ORDER BY dtInicio");
				break;
			case 101:
				stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Turma WHERE (status=0 OR status=2) AND idProfessor=? ORDER BY dtInicio");
				break;
			case 110:
				stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Turma WHERE (status=0 OR status=1) AND idProfessor=? ORDER BY dtInicio");
				break;
			default:
				stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Turma WHERE idProfessor=? ORDER BY dtInicio");
			}
			stmt.setInt(1, idProfessor);
			rs = myCONN.getResultSet(stmt);
			while (rs.next()) {
				Turma t = buildObject(rs);
				turmas.add(t);
			}
			rs.close();
		} catch (SQLException ex) {
			Logger.getLogger(TurmaDAO.class.getName()).log(Level.SEVERE, null, ex);
		}
		return turmas;
	}
	
	public ArrayList<Object> retrieveByStatusBuscaLikeCurso(String nomeCurso, int statusBusca) {
		PreparedStatement stmt;
		ArrayList<Object> turmas = new ArrayList<Object>();
		ResultSet rs;
		try {
			switch(statusBusca){
			case 1:
				stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Turma INNER JOIN Curso ON Turma.idCurso = Curso.id WHERE Curso.nome LIKE '%"+nomeCurso+"%' AND Turma.status=2 ORDER BY dtInicio");
				break;
			case 10:
				stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Turma INNER JOIN Curso ON Turma.idCurso = Curso.id WHERE Curso.nome LIKE '%"+nomeCurso+"%' AND Turma.status=1 ORDER BY dtInicio");
				break;
			case 11:
				stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Turma INNER JOIN Curso ON Turma.idCurso = Curso.id WHERE Curso.nome LIKE '%"+nomeCurso+"%' AND (Turma.status=1 OR Turma.status=2) ORDER BY dtInicio");
				break;
			case 100:
				stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Turma INNER JOIN Curso ON Turma.idCurso = Curso.id WHERE Curso.nome LIKE '%"+nomeCurso+"%' AND Turma.status=0 ORDER BY dtInicio");
				break;
			case 101:
				stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Turma INNER JOIN Curso ON Turma.idCurso = Curso.id WHERE Curso.nome LIKE '%"+nomeCurso+"%' AND (Turma.status=0 OR Turma.status=2) ORDER BY dtInicio");
				break;
			case 110:
				stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Turma INNER JOIN Curso ON Turma.idCurso = Curso.id WHERE Curso.nome LIKE '%"+nomeCurso+"%' AND (Turma.status=0 OR Turma.status=1) ORDER BY dtInicio");
				break;
			default:
				stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Turma INNER JOIN Curso ON Turma.idCurso = Curso.id WHERE Curso.nome LIKE '%"+nomeCurso+"%' ORDER BY dtInicio");
			}
			rs = myCONN.getResultSet(stmt);
			while (rs.next()) {
				Turma t = buildObject(rs);
				turmas.add(t);
			}
			rs.close();
		} catch (SQLException ex) {
			Logger.getLogger(TurmaDAO.class.getName()).log(Level.SEVERE, null, ex);
		}
		return turmas;
	}
	
	public ArrayList<Object> retrieveByStatusBuscaLikeProfessor(String nomeProfessor, int statusBusca) {
		PreparedStatement stmt;
		ArrayList<Object> turmas = new ArrayList<Object>();
		ResultSet rs;
		try {
			switch(statusBusca){
			case 1:
				stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Turma INNER JOIN Pessoa ON Turma.idProfessor = Pessoa.id WHERE Pessoa.nome LIKE '%"+nomeProfessor+"%' AND Turma.status=2 ORDER BY dtInicio");
				break;
			case 10:
				stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Turma INNER JOIN Pessoa ON Turma.idProfessor = Pessoa.id WHERE Pessoa.nome LIKE '%"+nomeProfessor+"%' AND Turma.status=1 ORDER BY dtInicio");
				break;
			case 11:
				stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Turma INNER JOIN Pessoa ON Turma.idProfessor = Pessoa.id WHERE Pessoa.nome LIKE '%"+nomeProfessor+"%' AND (Turma.status=1 OR Turma.status=2) ORDER BY dtInicio");
				break;
			case 100:
				stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Turma INNER JOIN Pessoa ON Turma.idProfessor = Pessoa.id WHERE Pessoa.nome LIKE '%"+nomeProfessor+"%' AND Turma.status=0 ORDER BY dtInicio");
				break;
			case 101:
				stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Turma INNER JOIN Pessoa ON Turma.idProfessor = Pessoa.id WHERE Pessoa.nome LIKE '%"+nomeProfessor+"%' AND (Turma.status=0 OR Turma.status=2) ORDER BY dtInicio");
				break;
			case 110:
				stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Turma INNER JOIN Pessoa ON Turma.idProfessor = Pessoa.id WHERE Pessoa.nome LIKE '%"+nomeProfessor+"%' AND (Turma.status=0 OR Turma.status=1) ORDER BY dtInicio");
				break;
			default:
				stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Turma INNER JOIN Pessoa ON Turma.idProfessor = Pessoa.id WHERE Pessoa.nome LIKE '%"+nomeProfessor+"%' ORDER BY dtInicio");
			}
			rs = myCONN.getResultSet(stmt);
			while (rs.next()) {
				Turma t = buildObject(rs);
				turmas.add(t);
			}
			rs.close();
		} catch (SQLException ex) {
			Logger.getLogger(TurmaDAO.class.getName()).log(Level.SEVERE, null, ex);
		}
		return turmas;
	}
	
	public Turma retrieveLastId() {
		int id = myCONN.lastId("Turma", "id");
		return retrieveById(id);
	}

	public boolean update(Turma turma) {
		PreparedStatement stmt;
		try {
			stmt = myCONN.getConnection().prepareStatement("UPDATE Turma SET idCurso=?, descricao=?, idProfessor=?, dtInicio=?, dtFim=?, status=?, observacoes=?, aulasGeradas=? WHERE id = ?");
			stmt.setInt(1, turma.getIdCurso());
			stmt.setString(2, turma.getDescricao());
			stmt.setInt(3, turma.getIdProfessor());
			stmt.setDate(4, new java.sql.Date(turma.getDtInicio().getTimeInMillis()));
			stmt.setDate(5, new java.sql.Date(turma.getDtFim().getTimeInMillis()));
			stmt.setInt(6, turma.getStatus());
			stmt.setString(7,turma.getObservacoes());
			stmt.setInt(8, (turma.isAulasGeradas()?1:0));
			stmt.setInt(9, turma.getId());
			int update = myCONN.executeUpdate(stmt);

			if (update == 1) {
				return true;
			}
		} catch (SQLException ex) {
			Logger.getLogger(TurmaDAO.class.getName()).log(Level.SEVERE, null, ex);
		}
		return false;
	}

	public void delete(Turma turma) {
		PreparedStatement stmt;
		try {
			stmt = myCONN.getConnection().prepareStatement("DELETE FROM Turma WHERE id = ?");
			stmt.setInt(1, turma.getId());
			myCONN.executeUpdate(stmt);
		} catch (SQLException ex) {
			Logger.getLogger(TurmaDAO.class.getName()).log(Level.SEVERE, null, ex);
		}
	}   
}//TurmaDAO
