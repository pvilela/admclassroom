/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author prvilela
 */
public class HoraAulaDAO {

    private static HoraAulaDAO instance;
    private GenericCONN myCONN;

    private HoraAulaDAO() {
        try {
            Class<?> daoClass = Class.forName("model." + GenericCONN.DAOName);
            myCONN = (GenericCONN) daoClass.newInstance();
        } catch (Exception e) {
            System.out.println("Could not find DAO class: " + "model." + GenericCONN.DAOName);
        }
        myCONN.getConnection();
    }

    public static HoraAulaDAO getInstance() {
        if (instance == null) {
            instance = new HoraAulaDAO();
        }
        return instance;
    }

    // CRUD
    public void create(int idTurma, int idProfessor, double valor) {
        PreparedStatement stmt;
        HoraAula horaAula = null;
        try {
            stmt = myCONN.getConnection().prepareStatement("INSERT INTO HoraAula (idTurma, idProfessor, valor) VALUES (?,?,?)");
            stmt.setInt(1, idTurma);
            stmt.setInt(2, idProfessor);
            stmt.setDouble(3, valor);
            myCONN.executeUpdate(stmt);
        } catch (SQLException ex) {
            Logger.getLogger(HoraAulaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private HoraAula buildObject(ResultSet rs) {
        HoraAula horaAula = null;
        try {
            horaAula = new HoraAula(rs.getInt("idTurma"), rs.getInt("idProfessor"), rs.getDouble("valor"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return horaAula;
    }

    public HoraAula retrieveByIdTurmaIdProfessor(int idTurma, int idProfessor) {
        PreparedStatement stmt;
        HoraAula horaAula = null;
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM HoraAula WHERE idTurma=? AND idProfessor=?");
            stmt.setInt(1, idTurma);
            stmt.setInt(2, idProfessor);
            rs = myCONN.getResultSet(stmt);
            if (rs.next()) {
                horaAula = buildObject(rs);
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(HoraAulaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return horaAula;
    }

    public boolean update(HoraAula horaAula) {
        PreparedStatement stmt;
        try {
            stmt = myCONN.getConnection().prepareStatement("UPDATE HoraAula SET valor=? WHERE idTurma=? AND idProfessor=?");
            stmt.setDouble(1, horaAula.getValor());
            stmt.setInt(2, horaAula.getIdTurma());
            stmt.setInt(3, horaAula.getIdProfessor());
            int update = myCONN.executeUpdate(stmt);
            if (update == 1) {
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(HoraAulaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public void delete(HoraAula horaAula) {
        PreparedStatement stmt;
        try {
            stmt = myCONN.getConnection().prepareStatement("DELETE FROM HoraAula WHERE idTurma=? AND idProfessor=?");
            stmt.setInt(1, horaAula.getIdTurma());
            stmt.setInt(2, horaAula.getIdProfessor());
            myCONN.executeUpdate(stmt);
        } catch (SQLException ex) {
            Logger.getLogger(HoraAulaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
