/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author prvilela
 */
public class HoraAula {
    private int idTurma;
    private int idProfessor;
    private double valor;

    public HoraAula(int idTurma, int idProfessor, double valor) {
        this.idTurma = idTurma;
        this.idProfessor = idProfessor;
        this.valor = valor;
    }

    public int getIdTurma() {
        return idTurma;
    }

    public void setIdTurma(int idTurma) {
        this.idTurma = idTurma;
    }

    public int getIdProfessor() {
        return idProfessor;
    }

    public void setIdProfessor(int idProfessor) {
        this.idProfessor = idProfessor;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
    
}
