package model;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * @author Prof. Dr. Plinio Vilela - vilela@ydoo.com.br
 */
public class MensagemDAO {

    private static MensagemDAO instance;
    private GenericCONN myCONN;
    private static final String SMTP_HOST_NAME = "smtp.itelefonica.com.br";
    private static final String SMTP_AUTH_USER = "pliniovilela@itelefonica.com.br";
    private static final String SMTP_AUTH_PWD = "m0nk3y";

    private MensagemDAO() {
        try {
            Class<?> daoClass = Class.forName("model." + GenericCONN.DAOName);
            myCONN = (GenericCONN) daoClass.newInstance();
        } catch (Exception e) {
            System.out.println("Could not find DAO class: " + "model." + GenericCONN.DAOName);
        }
        myCONN.getConnection();
    }

    public static MensagemDAO getInstance() {
        if (instance == null) {
            instance = new MensagemDAO();
        }
        return instance;
    }

    private class SMTPAuthenticator extends javax.mail.Authenticator {

        public PasswordAuthentication getPasswordAuthentication() {
            String username = SMTP_AUTH_USER;
            String password = SMTP_AUTH_PWD;
            return new PasswordAuthentication(username, password);
        }
    }

    private boolean enviaEMail(int idUsuarioOrigem, int idUsuarioDestino, String assunto, String mensagem) throws Exception {

        Usuario userOrigem = UsuarioDAO.getInstance().retrieveById(idUsuarioOrigem);
        Usuario userDestino = UsuarioDAO.getInstance().retrieveById(idUsuarioDestino);
        Pessoa pessoaOrigem = PessoaDAO.getInstance().retrieveById(userOrigem.getIdPessoa());
        Pessoa pessoaDestino = PessoaDAO.getInstance().retrieveById(userDestino.getIdPessoa());

        if (!pessoaDestino.getEmail().equals("")) {
            Properties p = new Properties();
            p.put("mail.smtp.port", 587);
            p.put("mail.smtp.starttls.enable", "true");
            p.put("mail.transport.protocol", "smtp");
            p.put("mail.smtp.host", SMTP_HOST_NAME);
            p.put("mail.smtp.auth", "true");


            Authenticator auth = new SMTPAuthenticator();
            Session mailSession = Session.getDefaultInstance(p, auth);
            // uncomment for debugging infos to stdout
            //mailSession.setDebug(true);
            Transport transport = mailSession.getTransport();

            MimeMessage message = new MimeMessage(mailSession);

            String heading = new String("Atenção:\n"+pessoaOrigem.getNome()+", lhe enviou uma menagem pelo Sistema de Administração Escolar People Sousas.\n======================================\n\n\n");
            String tail = new String("\n======================================\n");

            message.setContent(heading+mensagem+tail, "text/plain");
            //message.setFrom(new InternetAddress((pessoaOrigem.getEmail().equals("")) ? "suporte@peoplesousas.com.br" : pessoaOrigem.getEmail()));
            message.setFrom(new InternetAddress("pliniovilela@itelefonica.com.br"));
            message.addRecipient(Message.RecipientType.TO,
                    new InternetAddress(pessoaDestino.getEmail()));
            message.setSentDate(Calendar.getInstance().getTime());
            message.setSubject(assunto, "text/plain");
            transport.connect();
            transport.sendMessage(message,
                    message.getRecipients(Message.RecipientType.TO));
            transport.close();

        }
        return false;
    }

    // CRUD
    public void create(int idUsuarioOrigem, int idUsuarioDestino, String assunto, String mensagem) {
        Calendar today = Calendar.getInstance();

        try {
            this.enviaEMail(idUsuarioOrigem, idUsuarioDestino, assunto, mensagem);
        } catch (Exception ex) {
            Logger.getLogger(MensagemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        PreparedStatement stmt;
        try {
            stmt = myCONN.getConnection().prepareStatement("INSERT INTO Mensagem (idUsuarioOrigem, idUsuarioDestino, assunto, mensagem, dataHora) VALUES (?,?,?,?,?)");
            stmt.setInt(1, idUsuarioOrigem);
            stmt.setInt(2, idUsuarioDestino);
            stmt.setString(3, assunto);
            stmt.setString(4, mensagem);
            stmt.setDate(5, new java.sql.Date(today.getTimeInMillis()));
            myCONN.executeUpdate(stmt);
        } catch (SQLException ex) {
            Logger.getLogger(MensagemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private Mensagem buildObject(ResultSet rs) {
        Calendar dt = Calendar.getInstance();
        Mensagem mensagem = null;
        try {
            dt.setTime(rs.getDate("dataHora"));
            mensagem = new Mensagem(rs.getInt("id"), rs.getInt("idUsuarioOrigem"), rs.getInt("idUsuarioDestino"), rs.getString("assunto"), rs.getString("mensagem"), dt, rs.getInt("status"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return mensagem;
    }

    public ArrayList<Mensagem> retrieveAll() {
        PreparedStatement stmt;
        ArrayList<Mensagem> mensagens = new ArrayList<Mensagem>();
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Mensagem ORDER BY dataHora DESC");
            rs = myCONN.getResultSet(stmt);
            while (rs.next()) {
                Mensagem t = buildObject(rs);
                mensagens.add(t);
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(MensagemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return mensagens;
    }

    public Mensagem retrieveById(int id) {
        PreparedStatement stmt;
        Mensagem mensagem = null;
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Mensagem WHERE id=?");
            stmt.setInt(1, id);
            rs = myCONN.getResultSet(stmt);
            if (rs.next()) {
                mensagem = buildObject(rs);
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(MensagemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return mensagem;
    }

    public ArrayList<Object> retrieveByUsuarioDestino(int idUsuarioDestino) {
        PreparedStatement stmt;
        ArrayList<Object> mensagens = new ArrayList<Object>();
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Mensagem WHERE idUsuarioDestino=? ORDER BY dataHora DESC");
            stmt.setInt(1, idUsuarioDestino);
            rs = myCONN.getResultSet(stmt);
            while (rs.next()) {
                Mensagem t = buildObject(rs);
                mensagens.add(t);
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(MensagemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return mensagens;
    }

    public ArrayList<Object> retrieveByUsuarioOrigem(int idUsuarioOrigem) {
        PreparedStatement stmt;
        ArrayList<Object> mensagens = new ArrayList<Object>();
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Mensagem WHERE idUsuarioOrigem=? ORDER BY dataHora DESC");
            stmt.setInt(1, idUsuarioOrigem);
            rs = myCONN.getResultSet(stmt);
            while (rs.next()) {
                Mensagem t = buildObject(rs);
                mensagens.add(t);
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(MensagemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return mensagens;
    }

    public ArrayList<Object> retrieveByUsuarioDestinoStatus(int idUsuarioDestino, int status) {
        PreparedStatement stmt;
        ArrayList<Object> mensagens = new ArrayList<Object>();
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Mensagem WHERE idUsuarioDestino=?, status=? ORDER BY dataHora DESC");
            stmt.setInt(1, idUsuarioDestino);
            stmt.setInt(2, status);
            rs = myCONN.getResultSet(stmt);
            while (rs.next()) {
                Mensagem t = buildObject(rs);
                mensagens.add(t);
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(MensagemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return mensagens;
    }

    public Mensagem retrieveLastId() {
        int id = myCONN.lastId("Mensagem", "id");
        return retrieveById(id);
    }

    public boolean update(Mensagem mensagem) {
        PreparedStatement stmt;
        try {
            stmt = myCONN.getConnection().prepareStatement("UPDATE Mensagem SET status=? WHERE id = ?");
            stmt.setInt(1, mensagem.getStatus());
            stmt.setInt(2, mensagem.getId());
            int update = myCONN.executeUpdate(stmt);

            if (update == 1) {
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(MensagemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public void delete(Mensagem mensagem) {
        PreparedStatement stmt;
        try {
            stmt = myCONN.getConnection().prepareStatement("DELETE FROM Mensagem WHERE id = ?");
            stmt.setInt(1, mensagem.getId());
            myCONN.executeUpdate(stmt);
        } catch (SQLException ex) {
            Logger.getLogger(MensagemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
