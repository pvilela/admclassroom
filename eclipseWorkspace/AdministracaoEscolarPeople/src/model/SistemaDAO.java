/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author prvilela
 */
public class SistemaDAO {

    private static SistemaDAO instance;
    private GenericCONN myCONN;

    private SistemaDAO() {
        try {
            Class<?> daoClass = Class.forName("model." + GenericCONN.DAOName);
            myCONN = (GenericCONN) daoClass.newInstance();
        } catch (Exception e) {
            System.out.println("Could not find DAO class: " + "model." + GenericCONN.DAOName);
        }
        myCONN.getConnection();
    }

    public static SistemaDAO getInstance() {
        if (instance == null) {
            instance = new SistemaDAO();
        }
        return instance;
    }

    // CRUD
    public void create(int versao, String observacao) {
        PreparedStatement stmt;
        Calendar data = Calendar.getInstance();
        try {
            stmt = myCONN.getConnection().prepareStatement("INSERT INTO Sistema (versao, descricao, data) VALUES (?,?,?)");
            stmt.setInt(1, versao);
            stmt.setString(2, observacao);
            stmt.setDate(3, new java.sql.Date(data.getTimeInMillis()));
            myCONN.executeUpdate(stmt);
        } catch (SQLException ex) {
            Logger.getLogger(SistemaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private Sistema buildObject(ResultSet rs) {
        Sistema sistema = null;
        java.sql.Date dt;
        Calendar cal = Calendar.getInstance();
        try {
            dt = rs.getDate("data");
            cal.setTime(dt);
            sistema = new Sistema(rs.getInt("id"), rs.getInt("versao"), rs.getString("descricao"), cal);

        } catch (SQLException ex) {
            Logger.getLogger(SistemaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sistema;
    }


    private Sistema retrieveById(int id) {
        PreparedStatement stmt;
        Sistema sistema = null;
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Sistema WHERE id=?");
            stmt.setInt(1, id);
            rs = myCONN.getResultSet(stmt);
            if (rs.next()) {
                sistema = buildObject(rs);
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(SistemaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sistema;
    }

    public Sistema retrieveLastId() {
        int id = myCONN.lastId("Sistema", "id");
        return retrieveById(id);
    }

}
