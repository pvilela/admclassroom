package model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Prof. Dr. Plinio Vilela - vilela@ydoo.com.br
 */
public class AulaDAO {

    private static AulaDAO instance;
    private GenericCONN myCONN;

    private AulaDAO() {
        try {
            Class<?> daoClass = Class.forName("model." + GenericCONN.DAOName);
            myCONN = (GenericCONN) daoClass.newInstance();
        } catch (Exception e) {
            System.out.println("Could not find DAO class: " + "model." + GenericCONN.DAOName);
        }
        myCONN.getConnection();
    }

    public static AulaDAO getInstance() {
        if (instance == null) {
            instance = new AulaDAO();
        }
        return instance;
    }

    // CRUD
    public Aula create(int numero, int idTurma, Calendar data, int cargaHoraria) {
        PreparedStatement stmt;
        Aula aula = null;
        try {
            stmt = myCONN.getConnection().prepareStatement("INSERT INTO Aula (numero, idTurma, data, cargaHoraria) VALUES (?,?,?,?)");
            stmt.setInt(1, numero);
            stmt.setInt(2, idTurma);
            stmt.setDate(3, new java.sql.Date(data.getTimeInMillis()));
            stmt.setInt(4, cargaHoraria);
            myCONN.executeUpdate(stmt);
            aula = retrieveLastId();
        } catch (SQLException ex) {
            Logger.getLogger(AulaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return aula;
    }

    private Aula buildObject(ResultSet rs) {
        Calendar dt = Calendar.getInstance();
        Aula aula = null;
        try {
            dt.setTime(rs.getDate("data"));
            aula = new Aula(rs.getInt("id"), rs.getInt("numero"), rs.getInt("idTurma"), rs.getInt("idProfessor"), dt, rs.getInt("cargaHoraria"), rs.getString("conteudoPrevisto"), rs.getString("conteudoMinistrado"), rs.getInt("status"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return aula;
    }

    public ArrayList<Object> retrieveByIdTurma(int idTurma) {
        PreparedStatement stmt;
        ArrayList<Object> aulas = new ArrayList<Object>();
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Aula WHERE idTurma=? ORDER BY data");
            stmt.setInt(1, idTurma);
            rs = myCONN.getResultSet(stmt);
            while (rs.next()) {
                Aula t = buildObject(rs);
                aulas.add(t);
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(AulaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return aulas;
    }

    public ArrayList<Object> retrieveAulasPendentesByIdTurma(int idTurma) {
        PreparedStatement stmt;
        ArrayList<Object> aulas = new ArrayList<Object>();
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Aula WHERE idTurma=? AND status=0 ORDER BY data");
            stmt.setInt(1, idTurma);
            rs = myCONN.getResultSet(stmt);
            while (rs.next()) {
                Aula t = buildObject(rs);
                aulas.add(t);
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(AulaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return aulas;
    }

    public ArrayList<Object> retrieveAulasDadasByIdProfessorMes(int idProfessor, int mes, int ano) {
        PreparedStatement stmt;
        ArrayList<Object> aulas = new ArrayList<Object>();
        ResultSet rs;
        try {
            //stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Aula INNER JOIN Turma ON Aula.idTurma = Turma.id WHERE Turma.idProfessor=? AND Aula.status=1 AND MONTH(Aula.data)=? AND YEAR(Aula.data)=? ORDER BY Aula.data");
    stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Aula WHERE idProfessor=? AND status=1 AND MONTH(data)=? AND YEAR(data)=? ORDER BY data");
            stmt.setInt(1, idProfessor);
            stmt.setInt(2, mes);
            stmt.setInt(3, ano);
            rs = myCONN.getResultSet(stmt);
            while (rs.next()) {
                Aula t = buildObject(rs);
                aulas.add(t);
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(AulaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return aulas;
    }

    public ArrayList<Object> retrieveAulasAfterCancelledAula(Aula aula) {
        PreparedStatement stmt;
        ArrayList<Object> aulas = new ArrayList<Object>();
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Aula WHERE idTurma=? AND data > ? ORDER BY numero");
            stmt.setInt(1, aula.getIdTurma());
            stmt.setDate(2, new java.sql.Date(aula.getData().getTimeInMillis()));
            rs = myCONN.getResultSet(stmt);
            while (rs.next()) {
                Aula t = buildObject(rs);
                aulas.add(t);
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(AulaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return aulas;
    }

    public Aula retrieveById(int id) {
        PreparedStatement stmt;
        Aula aula = null;
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Aula WHERE id=?");
            stmt.setInt(1, id);
            rs = myCONN.getResultSet(stmt);
            if (rs.next()) {
                aula = buildObject(rs);
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(AulaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return aula;
    }


    public Aula retrieveUltimaAulaByIdTurma(int idTurma) {
        PreparedStatement stmt;
        Aula aula = null;
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT MAX(id) AS \"key\" FROM Aula WHERE idTurma=?");
            stmt.setInt(1, idTurma);
            rs = myCONN.getResultSet(stmt);
            if (rs.next()) {
                return retrieveById(rs.getInt("key"));
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(AulaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return aula;
    }

    public Aula retrieveLastId() {
        int id = myCONN.lastId("Aula", "id");
        return retrieveById(id);
    }

    public boolean update(Aula aula) {
        PreparedStatement stmt;
        try {
            stmt = myCONN.getConnection().prepareStatement("UPDATE Aula SET data=?, numero=?, idProfessor=?, conteudoPrevisto=?, conteudoMinistrado=?, cargaHoraria=?, status=? WHERE id = ?");
            stmt.setDate(1, new java.sql.Date(aula.getData().getTimeInMillis()));
            stmt.setInt(2, aula.getNumero());
            stmt.setInt(3, aula.getIdProfessor());
            stmt.setString(4, aula.getConteudoPrevisto());
            stmt.setString(5, aula.getConteudoMinistrado());
            stmt.setInt(6, aula.getCargaHoraria());
            stmt.setInt(7, aula.getStatus());
            stmt.setInt(8, aula.getId());
            int update = myCONN.executeUpdate(stmt);
            if (update == 1) {
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(AulaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public void delete(Aula aula) {
        PreparedStatement stmt;
        try {
            stmt = myCONN.getConnection().prepareStatement("DELETE FROM Aula WHERE id = ?");
            stmt.setInt(1, aula.getId());
            myCONN.executeUpdate(stmt);
        } catch (SQLException ex) {
            Logger.getLogger(AulaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}// AulaDAO
