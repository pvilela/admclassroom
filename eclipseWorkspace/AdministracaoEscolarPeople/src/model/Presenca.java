package model;

/**
 * @author Prof. Dr. Plinio Vilela - vilela@ydoo.com.br
 */
public class Presenca {
	private int id;
	private int idAula;
	private int idAluno;
	private int presenca;
	private String observacao;
	public Presenca(int id, int idAula, int idAluno, int presenca,
			String observacao) {
		super();
		this.id = id;
		this.idAula = idAula;
		this.idAluno = idAluno;
		this.presenca = presenca;
		this.observacao = observacao;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIdAula() {
		return idAula;
	}
	public void setIdAula(int idAula) {
		this.idAula = idAula;
	}
	public int getIdAluno() {
		return idAluno;
	}
	public void setIdAluno(int idAluno) {
		this.idAluno = idAluno;
	}
	public int getPresenca() {
		return presenca;
	}
	public void setPresenca(int presenca) {
		this.presenca = presenca;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
}// Presenca
