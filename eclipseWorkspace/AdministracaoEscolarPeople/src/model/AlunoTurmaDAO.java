package model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Prof. Dr. Plinio Vilela - vilela@ydoo.com.br
 */
public class AlunoTurmaDAO {

    private static AlunoTurmaDAO instance;
    private GenericCONN myCONN;

    private AlunoTurmaDAO() {
        try {
            Class<?> daoClass = Class.forName("model." + GenericCONN.DAOName);
            myCONN = (GenericCONN) daoClass.newInstance();
        } catch (Exception e) {
            System.out.println("Could not find DAO class: " + "model." + GenericCONN.DAOName);
        }
        myCONN.getConnection();
    }

    public static AlunoTurmaDAO getInstance() {
        if (instance == null) {
            instance = new AlunoTurmaDAO();
        }
        return instance;
    }

    // CRUD
    public void create(int idAluno, int idTurma) {
        Calendar today = Calendar.getInstance();
        PreparedStatement stmt;
        try {
            stmt = myCONN.getConnection().prepareStatement("INSERT INTO AlunoTurma (idAluno, idTurma, dtMatricula) VALUES (?,?,?)");
            stmt.setInt(1, idAluno);
            stmt.setInt(2, idTurma);
            stmt.setDate(3, new java.sql.Date(today.getTimeInMillis()));
            myCONN.executeUpdate(stmt);
        } catch (SQLException ex) {
            Logger.getLogger(AlunoTurmaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private AlunoTurma buildObject(ResultSet rs) {
        AlunoTurma alunoTurma = null;
        Calendar dtMatricula, dtCancelamento;
        dtMatricula = Calendar.getInstance();
        dtCancelamento = Calendar.getInstance();
        try {
            Date dateMatricula = rs.getDate("dtMatricula");
            Date dateCancelamento = rs.getDate("dtCancelamento");
            if (dateMatricula != null) {
                dtMatricula.setTime(dateMatricula);
            }
            if (dateCancelamento != null) {
                dtCancelamento.setTime(dateCancelamento);
            } else {
                dtCancelamento = null;
            }
            alunoTurma = new AlunoTurma(rs.getInt("idAluno"), rs.getInt("idTurma"), dtMatricula, dtCancelamento, rs.getInt("status"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return alunoTurma;
    }

    public ArrayList<AlunoTurma> retrieveByIdTurma(int idTurma) {
        PreparedStatement stmt;
        ArrayList<AlunoTurma> alunoTurmas = new ArrayList<AlunoTurma>();
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM AlunoTurma WHERE idTurma=? ORDER BY idAluno");
            stmt.setInt(1, idTurma);
            rs = myCONN.getResultSet(stmt);
            while (rs.next()) {
                AlunoTurma t = buildObject(rs);
                alunoTurmas.add(t);
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(AlunoTurmaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return alunoTurmas;
    }
    
        public ArrayList<Object> retrieveByIdAluno(int idAluno) {
        PreparedStatement stmt;
        ArrayList<Object> alunoTurmas = new ArrayList<Object>();
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM AlunoTurma WHERE idAluno=? ORDER BY idTurma");
            stmt.setInt(1, idAluno);
            rs = myCONN.getResultSet(stmt);
            while (rs.next()) {
                AlunoTurma t = buildObject(rs);
                alunoTurmas.add(t);
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(AlunoTurmaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return alunoTurmas;
    }

    public ArrayList<Object> retrieveAlunosByIdTurma(int idTurma) {
        PreparedStatement stmt;
        ArrayList<Object> alunosTurma = new ArrayList<Object>();
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM AlunoTurma INNER JOIN Pessoa ON Pessoa.id = AlunoTurma.idAluno WHERE AlunoTurma.idTurma=? ORDER BY Pessoa.nome");
            stmt.setInt(1, idTurma);
            rs = myCONN.getResultSet(stmt);
            while (rs.next()) {
                Pessoa p = PessoaDAO.getInstance().retrieveById(rs.getInt("AlunoTurma.idAluno"));
                alunosTurma.add(p);
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(AlunoTurmaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return alunosTurma;
    }

    public AlunoTurma retrieveByIdAlunoIdTurma(int idAluno, int idTurma) {
        PreparedStatement stmt;
        AlunoTurma alunoTurma = null;
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM AlunoTurma WHERE idAluno=? AND idTurma=?");
            stmt.setInt(1, idAluno);
            stmt.setInt(2, idTurma);
            rs = myCONN.getResultSet(stmt);
            if (rs.next()) {
                alunoTurma = buildObject(rs);
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(AlunoTurmaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return alunoTurma;
    }

    public boolean update(AlunoTurma alunoTurma) {
        PreparedStatement stmt;
        try {
            stmt = myCONN.getConnection().prepareStatement("UPDATE AlunoTurma SET dtMatricula=?, dtCancelamento=?, status=? WHERE idAluno=? AND idTurma=?");
            stmt.setDate(1, new java.sql.Date(alunoTurma.getDtMatricula().getTimeInMillis()));
            if (alunoTurma.getDtCancelamento() != null) {
                stmt.setDate(2, new java.sql.Date(alunoTurma.getDtCancelamento().getTimeInMillis()));
            } else {
                stmt.setDate(2, null);
            }
            stmt.setInt(3, alunoTurma.getStatus());
            stmt.setInt(4, alunoTurma.getIdAluno());
            stmt.setInt(5, alunoTurma.getIdTurma());
            int update = myCONN.executeUpdate(stmt);
            if (update == 1) {
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(AlunoTurmaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public void delete(AlunoTurma alunoTurma) {
        PreparedStatement stmt;
        try {
            stmt = myCONN.getConnection().prepareStatement("DELETE FROM AlunoTurma WHERE idAluno=? AND idTurma=?");
            stmt.setInt(1, alunoTurma.getIdAluno());
            stmt.setInt(2, alunoTurma.getIdTurma());
            myCONN.executeUpdate(stmt);
        } catch (SQLException ex) {
            Logger.getLogger(AlunoTurmaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void delete(int idAluno, int idTurma) {
        PreparedStatement stmt;
        try {
            stmt = myCONN.getConnection().prepareStatement("DELETE FROM AlunoTurma WHERE idAluno=? AND idTurma=?");
            stmt.setInt(1, idAluno);
            stmt.setInt(2, idTurma);
            myCONN.executeUpdate(stmt);
        } catch (SQLException ex) {
            Logger.getLogger(AlunoTurmaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}// AlunoTurmaDAO
