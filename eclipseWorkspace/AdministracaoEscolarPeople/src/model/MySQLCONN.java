package model;
import java.sql.Connection;
import java.sql.DriverManager;
import javax.swing.JOptionPane;
/**
 * @author Prof. Dr. Plinio Vilela - vilela@ydoo.com.br Para utilizar o banco
 * MySQL modifique a string: DAOName na classe GenericCONN.java
 */
public class MySQLCONN extends GenericCONN {
    public static final String DRIVER = "com.mysql.jdbc.Driver";
    //Modifique a String abaixo para indicar o nome do Banco de Dados apropriado:
    //Modifique as Strings abaixo para indicar o seu username e password no banco de dados:
    //private static String DBURL = "jdbc:mysql://www.ydoo.com.br/ydoo_SisEscola";
    //private static final String DBURL = "jdbc:mysql://192.168.0.196/ydoo_sisescola";
    //private static final String user = "ydoo_SisEscola";
    //private static final String pass = "51535c0l@";

    //Local
    private static String DBURL = "jdbc:mysql:///dbSistemaEscola";
    private static final String user = "vilela";
    private static final String pass = "vilela";

    //MacMini1
    //private static String DBURL = "jdbc:mysql://192.168.0.108/dbSistemaEscola";
    //private static final String user = "vilela";
    //private static final String pass = "vilela";

    // metodo para criar a conexao com o Banco
    @Override
    public Connection getConnection() {
        while (con == null) {
            try {
                //Class.forName(DRIVER).newInstance();
                con = DriverManager.getConnection(DBURL, user, pass);
            } catch (Exception e) {
                //JOptionPane.showMessageDialog(null, "Não consegui acessar o banco em: "+DBURL+"\nVou tentar localmente.");
                String[] choices = {"Sim, tenho um banco de dados local.", "Não, apenas encerre."};
                int resp = JOptionPane.showOptionDialog(
                        null // Center in window.
                        , "Não consegui acessar o banco em:\n"+DBURL+"\nDevo tentar o banco local?" // Message
                        , "Acesso ao Banco de Dados" // Title in titlebar
                        , JOptionPane.YES_NO_OPTION // Option type
                        , JOptionPane.PLAIN_MESSAGE // messageType
                        , null // Icon (none)
                        , choices // Button text as above.
                        , "" // Default button's label
                        );
                if(resp==0){
                    DBURL = "jdbc:mysql:///dbSistemaEscola";
                }else{
                    System.exit(0);
                    //System.err.println("Exception: " + e.getMessage());
                }
            }
        }
        return con;
    }
}