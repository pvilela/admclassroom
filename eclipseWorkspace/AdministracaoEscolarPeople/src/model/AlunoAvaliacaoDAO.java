package model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Prof. Dr. Plinio Vilela - vilela@ydoo.com.br
 */
public class AlunoAvaliacaoDAO {

    private static AlunoAvaliacaoDAO instance;
    private GenericCONN myCONN;

    private AlunoAvaliacaoDAO() {
        try {
            Class<?> daoClass = Class.forName("model." + GenericCONN.DAOName);
            myCONN = (GenericCONN) daoClass.newInstance();
        } catch (Exception e) {
            System.out.println("Could not find DAO class: " + "model." + GenericCONN.DAOName);
        }
        myCONN.getConnection();
    }

    public static AlunoAvaliacaoDAO getInstance() {
        if (instance == null) {
            instance = new AlunoAvaliacaoDAO();
        }
        return instance;
    }

    // CRUD
    public void create(int idAluno, int idAvaliacao, double nota) {
        PreparedStatement stmt;
        try {
            stmt = myCONN.getConnection().prepareStatement("INSERT INTO AlunoAvaliacao (idAluno, idAvaliacao, nota) VALUES (?,?,?)");
            stmt.setInt(1, idAluno);
            stmt.setInt(2, idAvaliacao);
            stmt.setDouble(3, nota);
            myCONN.executeUpdate(stmt);
        } catch (SQLException ex) {
            Logger.getLogger(AlunoAvaliacaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private AlunoAvaliacao buildObject(ResultSet rs) {
        AlunoAvaliacao alunoAvaliacao = null;
        try {
            alunoAvaliacao = new AlunoAvaliacao(rs.getInt("idAluno"), rs.getInt("idAvaliacao"), rs.getDouble("nota"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return alunoAvaliacao;
    }

    public ArrayList<AlunoAvaliacao> retrieveByIdAvaliacao(int idAvaliacao) {
        PreparedStatement stmt;
        ArrayList<AlunoAvaliacao> alunoAvaliacaos = new ArrayList<AlunoAvaliacao>();
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM AlunoAvaliacao WHERE idAvaliacao=?");
            stmt.setInt(1, idAvaliacao);
            rs = myCONN.getResultSet(stmt);
            while (rs.next()) {
                AlunoAvaliacao t = buildObject(rs);
                alunoAvaliacaos.add(t);
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(AlunoAvaliacaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return alunoAvaliacaos;
    }

    public AlunoAvaliacao retrieveByIdAlunoIdAvaliacao(int idAluno, int idAvaliacao) {
        PreparedStatement stmt;
        AlunoAvaliacao alunoAvaliacao = null;
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM AlunoAvaliacao WHERE idAluno=? AND idAvaliacao=?");
            stmt.setInt(1, idAluno);
            stmt.setInt(2, idAvaliacao);
            rs = myCONN.getResultSet(stmt);
            if (rs.next()) {
                alunoAvaliacao = buildObject(rs);
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(AlunoAvaliacaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return alunoAvaliacao;
    }

    public boolean update(AlunoAvaliacao alunoAvaliacao) {
        PreparedStatement stmt;
        System.out.println("Nota= "+alunoAvaliacao.getNota());
        try {
            stmt = myCONN.getConnection().prepareStatement("UPDATE AlunoAvaliacao SET nota=? WHERE idAluno=? AND idAvaliacao=?");
            stmt.setDouble(1, alunoAvaliacao.getNota());
            stmt.setInt(2, alunoAvaliacao.getIdAluno());
            stmt.setInt(3, alunoAvaliacao.getIdAvaliacao());
            int update = myCONN.executeUpdate(stmt);
            if (update == 1) {
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(AlunoAvaliacaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public void delete(int idAluno, int idAvaliacao) {
        PreparedStatement stmt;
        try {
            stmt = myCONN.getConnection().prepareStatement("DELETE FROM AlunoAvaliacao WHERE idAluno=? AND idAvaliacao=?");
            stmt.setInt(1, idAluno);
            stmt.setInt(2, idAvaliacao);
            myCONN.executeUpdate(stmt);
        } catch (SQLException ex) {
            Logger.getLogger(AlunoAvaliacaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteAllByIdAvaliacao(int idAvaliacao) {
        PreparedStatement stmt;
        try {
            stmt = myCONN.getConnection().prepareStatement("DELETE FROM AlunoAvaliacao WHERE idAvaliacao=?");
            stmt.setInt(1, idAvaliacao);
            myCONN.executeUpdate(stmt);
        } catch (SQLException ex) {
            Logger.getLogger(AlunoAvaliacaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}// AlunoAvaliacaoDAO
