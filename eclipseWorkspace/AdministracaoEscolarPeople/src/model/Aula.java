package model;

import java.util.Calendar;

/**
 * @author Prof. Dr. Plinio Vilela - vilela@ydoo.com.br
 */
public class Aula {
	private int id;
	private int numero;
	private int idTurma;
        private int idProfessor;
	private Calendar data;
	private int cargaHoraria;
	private String conteudoPrevisto;
	private String conteudoMinistrado;
	private int status;
	// 0 - prevista
	// 1 - ocorreu
        // 2 - cancelada
	public Aula(int id, int numero, int idTurma, int idProfessor, Calendar data,
			int cargaHoraria, String conteudoPrevisto,
			String conteudoMinistrado, int status) {
		super();
		this.id = id;
		this.numero = numero;
		this.idTurma = idTurma;
                this.idProfessor = idProfessor;
		this.data = data;
		this.cargaHoraria = cargaHoraria;
		this.conteudoPrevisto = conteudoPrevisto;
		this.conteudoMinistrado = conteudoMinistrado;
		this.status = status;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public int getIdTurma() {
		return idTurma;
	}
	public void setIdTurma(int idTurma) {
		this.idTurma = idTurma;
	}
        public int getIdProfessor(){
            return idProfessor;
        }
        public void setIdProfessor(int idProfessor){
            this.idProfessor = idProfessor;
        }
	public Calendar getData() {
		return data;
	}
	public void setData(Calendar data) {
		this.data = data;
	}
	public int getCargaHoraria() {
		return cargaHoraria;
	}
	public void setCargaHoraria(int cargaHoraria) {
		this.cargaHoraria = cargaHoraria;
	}
	public String getConteudoPrevisto() {
		return conteudoPrevisto;
	}
	public void setConteudoPrevisto(String conteudoPrevisto) {
		this.conteudoPrevisto = conteudoPrevisto;
	}
	public String getConteudoMinistrado() {
		return conteudoMinistrado;
	}
	public void setConteudoMinistrado(String conteudoMinistrado) {
		this.conteudoMinistrado = conteudoMinistrado;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
}// Aula
