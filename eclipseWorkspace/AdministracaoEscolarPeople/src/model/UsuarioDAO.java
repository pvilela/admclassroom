package model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jasypt.util.password.BasicPasswordEncryptor;

/**
 * @author Prof. Dr. Plinio Vilela - vilela@ydoo.com.br
 */
public class UsuarioDAO {
    private static UsuarioDAO instance;
    private GenericCONN myCONN;

    private UsuarioDAO() {
        try {
            Class<?> daoClass = Class.forName("model." + GenericCONN.DAOName);
            myCONN = (GenericCONN) daoClass.newInstance();
        } catch (Exception e) {
            System.out.println("Could not find DAO class: " + "model." + GenericCONN.DAOName);
        }
        myCONN.getConnection();
    }

    public static UsuarioDAO getInstance() {
        if (instance == null) {
            instance = new UsuarioDAO();
        }
        return instance;
    }

    // CRUD
    public void create(int idPessoa, String username, String senha) {    	
        PreparedStatement stmt;
        try {
            stmt = myCONN.getConnection().prepareStatement("INSERT INTO Usuario (idPessoa, username, senha) VALUES (?,?,?)");
            stmt.setInt(1, idPessoa);
            stmt.setString(2, username);
            stmt.setString(3, getEncryptedPassword(senha));            
            myCONN.executeUpdate(stmt);
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private String getEncryptedPassword(String userPassword){
       	BasicPasswordEncryptor passwordEncryptor = new BasicPasswordEncryptor();
    	return passwordEncryptor.encryptPassword(userPassword);  
    }
    
    public boolean checkPassword(String username, String inputPassword){
    	BasicPasswordEncryptor passwordEncryptor = new BasicPasswordEncryptor();
    	Usuario usuario = retrieveByUsername(username);
    	if(usuario!=null){
    		return passwordEncryptor.checkPassword(inputPassword, usuario.getSenha());
    	}
    	return false;
    }

    private Usuario buildObject(ResultSet rs){
    	Usuario usuario = null;
    	try {
			usuario = new Usuario(rs.getInt("id"), rs.getInt("idPessoa"), rs.getString("username"), rs.getString("senha"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
    	return usuario;
    }
    
    public ArrayList<Object> retrieveAll() {
        PreparedStatement stmt;
        ArrayList<Object> usuarios = new ArrayList<Object>();
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Usuario ORDER BY username");
            rs = myCONN.getResultSet(stmt);
            while (rs.next()) {
                usuarios.add(buildObject(rs));
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return usuarios;
    }
    
    public ArrayList<Object> retrieveAllAtivo() {
        PreparedStatement stmt;
        ArrayList<Object> usuarios = new ArrayList<Object>();
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Usuario U, Pessoa P WHERE U.idPessoa = P.id AND P.status = 1 ORDER BY U.username");
            rs = myCONN.getResultSet(stmt);
            while (rs.next()) {
                usuarios.add(buildObject(rs));
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return usuarios;
    }    
    

    public ArrayList<Usuario> retrieveLike(String username) {
        PreparedStatement stmt;
        ArrayList<Usuario> usuarios = new ArrayList<Usuario>();
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Usuario WHERE username LIKE '%"+username+"%' ORDER BY username");
            rs = myCONN.getResultSet(stmt);
            while (rs.next()) {
                usuarios.add(buildObject(rs));
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return usuarios;
    }

    public Usuario retrieveById(int id) {
        PreparedStatement stmt;
        Usuario usuario = null;
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Usuario where id=?");
            stmt.setInt(1, id);
            rs = myCONN.getResultSet(stmt);
            if (rs.next()) {
                usuario = buildObject(rs);
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return usuario;
    }
    
    public Usuario retrieveByIdPessoa(int idPessoa) {
        PreparedStatement stmt;
        Usuario usuario = null;
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Usuario where idPessoa=?");
            stmt.setInt(1, idPessoa);
            rs = myCONN.getResultSet(stmt);
            if (rs.next()) {
                usuario = buildObject(rs);
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return usuario;
    }    
    
    public Usuario retrieveByUsername(String username) {
        PreparedStatement stmt;
        Usuario usuario = null;
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Usuario where username=?");
            stmt.setString(1, username);
            rs = myCONN.getResultSet(stmt);
            if (rs.next()) {
                usuario = buildObject(rs);
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return usuario;
    }    

    public Usuario retrieveLastId() {
        int id = myCONN.lastId("Usuario", "id");
        return retrieveById(id);
    }

    public boolean update(Usuario usuario) {
    	// Nao atualiza a senha, utilize updateSenha.
        PreparedStatement stmt;
        try {
            stmt = myCONN.getConnection().prepareStatement("UPDATE Usuario SET idPessoa=?, username=? WHERE id = ?");
            stmt.setInt(1, usuario.getIdPessoa());
            stmt.setString(2, usuario.getUsername());
            stmt.setInt(3, usuario.getId());
            int update = myCONN.executeUpdate(stmt);
            if (update == 1) {
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public boolean updateSenha(int id, String senha) {
        PreparedStatement stmt;
        try {
            stmt = myCONN.getConnection().prepareStatement("UPDATE Usuario SET senha=? WHERE id = ?");
            stmt.setString(1, getEncryptedPassword(senha));
            stmt.setInt(2, id);
            int update = myCONN.executeUpdate(stmt);
            if (update == 1) {
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public void delete(Usuario usuario) {
        PreparedStatement stmt;
        try {
            stmt = myCONN.getConnection().prepareStatement("DELETE FROM Usuario WHERE id = ?");
            stmt.setInt(1, usuario.getId());
            myCONN.executeUpdate(stmt);
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}// UsuarioDAO
