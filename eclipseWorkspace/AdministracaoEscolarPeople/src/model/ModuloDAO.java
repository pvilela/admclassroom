package model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Prof. Dr. Plinio Vilela - vilela@ydoo.com.br
 */
public class ModuloDAO {
    private static ModuloDAO instance;
    private GenericCONN myCONN;

    private ModuloDAO() {
        try {
            Class<?> daoClass = Class.forName("model." + GenericCONN.DAOName);
            myCONN = (GenericCONN) daoClass.newInstance();
        } catch (Exception e) {
            System.out.println("Could not find DAO class: " + "model." + GenericCONN.DAOName);
        }
        myCONN.getConnection();
    }

    public static ModuloDAO getInstance() {
        if (instance == null) {
            instance = new ModuloDAO();
        }
        return instance;
    }

    // CRUD
    public void create(String nome, int cargaHoraria) {
        PreparedStatement stmt;
        try {
            stmt = myCONN.getConnection().prepareStatement("INSERT INTO Modulo (nome, cargaHoraria) VALUES (?,?)");
            stmt.setString(1, nome);
            stmt.setInt(2, cargaHoraria);
            myCONN.executeUpdate(stmt);
        } catch (SQLException ex) {
            Logger.getLogger(ModuloDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private Modulo buildObject(ResultSet rs){
    	Modulo modulo = null;
    	try {
			modulo = new Modulo(rs.getInt("id"), rs.getString("nome"), rs.getInt("cargaHoraria"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
    	return modulo;
    }

    public ArrayList<Object> retrieveAll() {
        PreparedStatement stmt;
        ArrayList<Object> modulos = new ArrayList<Object>();
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Modulo ORDER BY nome");
            rs = myCONN.getResultSet(stmt);
            while (rs.next()) {
                modulos.add(buildObject(rs));
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(ModuloDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return modulos;
    }

    public Modulo retrieveById(int id) {
        PreparedStatement stmt;
        Modulo tipoModulo = null;
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Modulo where id=?");
            stmt.setInt(1, id);
            rs = myCONN.getResultSet(stmt);
            if (rs.next()) {
                tipoModulo = buildObject(rs);
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(ModuloDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tipoModulo;
    } 

    public Modulo retrieveLastId() {
        int id = myCONN.lastId("Modulo", "id");
        return retrieveById(id);
    }

    public boolean update(Modulo modulo) {
        PreparedStatement stmt;
        try {
            stmt = myCONN.getConnection().prepareStatement("UPDATE Modulo SET nome=?, cargaHoraria=? WHERE id = ?");
            stmt.setString(1, modulo.getNome());
            stmt.setInt(2, modulo.getCargaHoraria());
            stmt.setInt(3, modulo.getId());
            int update = myCONN.executeUpdate(stmt);
            if (update == 1) {
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ModuloDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public void delete(Modulo modulo) {
        PreparedStatement stmt;
        try {
            stmt = myCONN.getConnection().prepareStatement("DELETE FROM Modulo WHERE id = ?");
            stmt.setInt(1, modulo.getId());
            myCONN.executeUpdate(stmt);
        } catch (SQLException ex) {
            Logger.getLogger(ModuloDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
