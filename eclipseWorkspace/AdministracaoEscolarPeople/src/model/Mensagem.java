package model;

import java.util.Calendar;

/**
 * @author Prof. Dr. Plinio Vilela - vilela@ydoo.com.br
 */
public class Mensagem {
	private int id;
	private int idUsuarioOrigem;
	private int idUsuarioDestino;
	private String assunto;
	private String mensagem;
	private Calendar dataHora;
	private int status;
	// 0 - nao lida
	// 1 - lida
	public Mensagem(int id, int idUsuarioOrigem, int idUsuarioDestino, String assunto,
			String mensagem, Calendar dataHora, int status) {
		super();
		this.id = id;
		this.idUsuarioOrigem = idUsuarioOrigem;
		this.idUsuarioDestino = idUsuarioDestino;
		this.assunto = assunto;
		this.mensagem = mensagem;
		this.dataHora = dataHora;
		this.status = status;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIdUsuarioOrigem() {
		return idUsuarioOrigem;
	}
	public void setIdUsuarioOrigem(int idUsuarioOrigem) {
		this.idUsuarioOrigem = idUsuarioOrigem;
	}
	public int getIdUsuarioDestino() {
		return idUsuarioDestino;
	}
	public void setIdUsuarioDestino(int idUsuarioDestino) {
		this.idUsuarioDestino = idUsuarioDestino;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	public Calendar getDataHora() {
		return dataHora;
	}
	public void setDataHora(Calendar dataHora) {
		this.dataHora = dataHora;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getAssunto() {
		return assunto;
	}
	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}	
}
