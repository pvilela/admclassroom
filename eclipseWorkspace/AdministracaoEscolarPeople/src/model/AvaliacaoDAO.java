package model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Prof. Dr. Plinio Vilela - vilela@ydoo.com.br
 */
public class AvaliacaoDAO {

    private static AvaliacaoDAO instance;
    private GenericCONN myCONN;

    private AvaliacaoDAO() {
        try {
            Class<?> daoClass = Class.forName("model." + GenericCONN.DAOName);
            myCONN = (GenericCONN) daoClass.newInstance();
        } catch (Exception e) {
            System.out.println("Could not find DAO class: " + "model." + GenericCONN.DAOName);
        }
        myCONN.getConnection();
    }

    public static AvaliacaoDAO getInstance() {
        if (instance == null) {
            instance = new AvaliacaoDAO();
        }
        return instance;
    }

    // CRUD
    public Avaliacao create(int idAula, String nome, String descricao, int peso) {
        PreparedStatement stmt;
        try {
            stmt = myCONN.getConnection().prepareStatement("INSERT INTO Avaliacao (idAula, nome, descricao, peso) VALUES (?,?,?,?)");
            stmt.setInt(1, idAula);
            stmt.setString(2, nome);
            stmt.setString(3, descricao);
            stmt.setInt(4, peso);
            myCONN.executeUpdate(stmt);
            return this.retrieveLastId();
        } catch (SQLException ex) {
            Logger.getLogger(AvaliacaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private Avaliacao buildObject(ResultSet rs) {
        Avaliacao avaliacao = null;
        try {
            avaliacao = new Avaliacao(rs.getInt("id"), rs.getInt("idAula"), rs.getString("nome"), rs.getString("descricao"), rs.getInt("peso"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return avaliacao;
    }

    public Avaliacao retrieveByIdAula(int idAula) {
        PreparedStatement stmt;
        Avaliacao avaliacao = null;
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Avaliacao WHERE idAula=?");
            stmt.setInt(1, idAula);
            rs = myCONN.getResultSet(stmt);
            if (rs.next()) {
                avaliacao = buildObject(rs);
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(AvaliacaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return avaliacao;
    }

    public Avaliacao retrieveById(int id) {
        PreparedStatement stmt;
        Avaliacao avaliacao = null;
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Avaliacao WHERE id=?");
            stmt.setInt(1, id);
            rs = myCONN.getResultSet(stmt);
            if (rs.next()) {
                avaliacao = buildObject(rs);
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(AvaliacaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return avaliacao;
    }

    public Avaliacao retrieveLastId() {
        int id = myCONN.lastId("Avaliacao", "id");
        return retrieveById(id);
    }

    public boolean update(Avaliacao avaliacao) {
        PreparedStatement stmt;
        try {
            stmt = myCONN.getConnection().prepareStatement("UPDATE Avaliacao SET nome=?, descricao=?, peso=? WHERE id=?");
            stmt.setString(1, avaliacao.getNome());
            stmt.setString(2, avaliacao.getDescricao());
            stmt.setInt(3, avaliacao.getPeso());
            stmt.setInt(4, avaliacao.getId());
            int update = myCONN.executeUpdate(stmt);
            if (update == 1) {
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(AvaliacaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public void delete(Avaliacao avaliacao) {
        PreparedStatement stmt;
        try {
            stmt = myCONN.getConnection().prepareStatement("DELETE FROM Avaliacao WHERE id = ?");
            stmt.setInt(1, avaliacao.getId());
            myCONN.executeUpdate(stmt);
        } catch (SQLException ex) {
            Logger.getLogger(AvaliacaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}// AvaliacaoDAO
