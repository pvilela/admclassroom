package model;
import java.sql.*;
/**
 * @author Prof. Dr. Plinio Vilela - vilela@ydoo.com.br
 */
public abstract class GenericCONN {
    // Altere essa String para utilizar outras DAOs:
    //public static final String DAOName = "EmbeddedDerbyDAO";
    public static final String DAOName = "MySQLCONN";

    protected static Connection con;

    public abstract Connection getConnection();

    protected ResultSet getResultSet(PreparedStatement queryStatement) throws SQLException {
        ResultSet rs;
        rs = queryStatement.executeQuery();
        return rs;
    }

    protected int executeUpdate(PreparedStatement queryStatement) throws SQLException {
        int update;
        update = queryStatement.executeUpdate();
        return update;
    }

    protected int lastId(String tableName, String primaryKey) {
        Statement s;
        ResultSet rs;
        int lastId = -1;
        try {
            con = this.getConnection();
            s = (Statement) con.createStatement();
            s.executeQuery("SELECT MAX(" + primaryKey + ") AS \"key\" FROM " + tableName);
            rs = s.getResultSet();
            if (rs.next()) {
                lastId = rs.getInt("key");
            }
        } catch (SQLException e) {
        }

        return lastId;
    }

    public void terminar() {
        try {
            (this.getConnection()).close();
        } catch (SQLException e) {
        }
    }
}