package model;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Prof. Dr. Plinio Vilela - vilela@ydoo.com.br
 */
public class PessoaDAO {

    private static PessoaDAO instance;
    private GenericCONN myCONN;

    private PessoaDAO() {
        try {
            Class<?> daoClass = Class.forName("model." + GenericCONN.DAOName);
            myCONN = (GenericCONN) daoClass.newInstance();
        } catch (Exception e) {
            System.out.println("Could not find DAO class: " + "model." + GenericCONN.DAOName);
        }
        myCONN.getConnection();
    }

    public static PessoaDAO getInstance() {
        if (instance == null) {
            instance = new PessoaDAO();
        }
        return instance;
    }

    // CRUD
    public void create(int tipo, String nome) {
        PreparedStatement stmt;
        try {
            stmt = myCONN.getConnection().prepareStatement("INSERT INTO Pessoa (tipo, nome) VALUES (?,?)");
            stmt.setInt(1, tipo);
            stmt.setString(2, nome);
            myCONN.executeUpdate(stmt);
        } catch (SQLException ex) {
            Logger.getLogger(PessoaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private Pessoa buildObject(ResultSet rs) {
        Pessoa pessoa = null;
        Calendar dt = Calendar.getInstance();

        try {
            if (rs.getDate("dataNascimento") != null) {
                dt.setTime(rs.getDate("dataNascimento"));
            } else {
                dt = null;
            }
            pessoa = new Pessoa(rs.getInt("id"), rs.getString("nome"), rs.getString("telefone"), rs.getString("email"), dt, rs.getInt("tipo"), rs.getInt("idResponsavel"), rs.getInt("status"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return pessoa;
    }

    private Pessoa buildObjectWithPicture(ResultSet rs) {
        Pessoa pessoa = null;
        Calendar dt = Calendar.getInstance();

        try {
            if (rs.getDate("dataNascimento") != null) {
                dt.setTime(rs.getDate("dataNascimento"));
            } else {
                dt = null;
            }
            pessoa = new Pessoa(rs.getInt("id"), rs.getString("nome"), rs.getString("telefone"), rs.getString("email"), dt, rs.getInt("tipo"), rs.getInt("idResponsavel"), rs.getInt("status"));
            byte[] imageData = rs.getBytes("foto");
            if (imageData != null) {
                InputStream in = new ByteArrayInputStream(imageData);
                pessoa.setFoto(in, imageData.length);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return pessoa;
    }    
    
    
    public ArrayList<Object> retrieveAll() {
        PreparedStatement stmt;
        ArrayList<Object> pessoas = new ArrayList<Object>();
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Pessoa ORDER BY nome");
            rs = myCONN.getResultSet(stmt);
            while (rs.next()) {
                pessoas.add(buildObject(rs));
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(PessoaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pessoas;
    }

    public ArrayList<Object> retrieveAllAtivos() {
        PreparedStatement stmt;
        ArrayList<Object> pessoas = new ArrayList<Object>();
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Pessoa WHERE status=1 ORDER BY nome");
            rs = myCONN.getResultSet(stmt);
            while (rs.next()) {
                pessoas.add(buildObject(rs));
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(PessoaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pessoas;
    }

    public ArrayList<Object> retrieveLike(String nome) {
        PreparedStatement stmt;
        ArrayList<Object> pessoas = new ArrayList<Object>();
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Pessoa WHERE nome LIKE '%" + nome + "%' ORDER BY nome");
            // Testar se funciona deste outro jeito:
            // stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Pessoa WHERE nome LIKE '%?%' ORDER BY nome");
            // stmt.setString(1,nome);
            rs = myCONN.getResultSet(stmt);
            while (rs.next()) {
                pessoas.add(buildObject(rs));
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(PessoaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pessoas;
    }

    public Pessoa retrieveById(int id) {
        PreparedStatement stmt;
        Pessoa pessoa = null;
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Pessoa WHERE id=?");
            stmt.setInt(1, id);
            rs = myCONN.getResultSet(stmt);
            if (rs.next()) {
                pessoa = buildObjectWithPicture(rs);
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(PessoaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pessoa;
    }

    public ArrayList<Object> retrieveAllByTipo(int tipo) {
        PreparedStatement stmt;
        ArrayList<Object> pessoas = new ArrayList<Object>();
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Pessoa WHERE tipo=? ORDER BY nome");
            stmt.setInt(1, tipo);
            rs = myCONN.getResultSet(stmt);
            while (rs.next()) {
                pessoas.add(buildObject(rs));
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(PessoaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pessoas;
    }

    public ArrayList<Object> retrieveAllAtivosByTipo(int tipo) {
        PreparedStatement stmt;
        ArrayList<Object> pessoas = new ArrayList<Object>();
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Pessoa WHERE tipo=? AND status=1 ORDER BY nome");
            stmt.setInt(1, tipo);
            rs = myCONN.getResultSet(stmt);
            while (rs.next()) {
                pessoas.add(buildObject(rs));
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(PessoaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pessoas;
    }

    public ArrayList<Object> retrieveAllAtivosByTipo(int tipo1, int tipo2) {
        PreparedStatement stmt;
        ArrayList<Object> pessoas = new ArrayList<Object>();
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Pessoa WHERE (tipo=? OR tipo=?) AND status=1 ORDER BY nome");
            stmt.setInt(1, tipo1);
            stmt.setInt(2, tipo2);
            rs = myCONN.getResultSet(stmt);
            while (rs.next()) {
                pessoas.add(buildObject(rs));
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(PessoaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pessoas;
    }

    public ArrayList<Object> retrieveAlunosAtivosNotInTurma(int idTurma) {
        PreparedStatement stmt;
        ArrayList<Object> alunosTurma = new ArrayList<Object>();
        ResultSet rs;
        try {
            stmt = myCONN.getConnection().prepareStatement("SELECT * FROM Pessoa WHERE id NOT IN (SELECT idAluno FROM AlunoTurma WHERE idTurma=?) AND (tipo = 4 OR tipo = 1 OR tipo = 0) AND (status = 1) ORDER BY nome");
            stmt.setInt(1, idTurma);
            rs = myCONN.getResultSet(stmt);
            while (rs.next()) {
                alunosTurma.add(buildObject(rs));
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(AlunoTurmaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return alunosTurma;
    }

    public Pessoa retrieveLastId() {
        int id = myCONN.lastId("Pessoa", "id");
        return retrieveById(id);
    }

    public boolean update(Pessoa pessoa) {
        PreparedStatement stmt;
        java.sql.Date sqlDate = null;
        try {
            if (pessoa.getDataNascimento() != null) {
                sqlDate = new java.sql.Date(pessoa.getDataNascimento().getTimeInMillis());
            }
            stmt = myCONN.getConnection().prepareStatement("UPDATE Pessoa SET nome=?, tipo=?, telefone=?, email=?, dataNascimento=?, idResponsavel=?, status=?, foto=? WHERE id = ?");
            stmt.setString(1, pessoa.getNome());
            stmt.setInt(2, pessoa.getTipo());
            stmt.setString(3, pessoa.getTelefone());
            stmt.setString(4, pessoa.getEmail());
            stmt.setDate(5, sqlDate);
            stmt.setInt(6, pessoa.getIdResponsavel());
            stmt.setInt(7, pessoa.getStatus());
            stmt.setBinaryStream(8, pessoa.getFoto(), (int) pessoa.size);
            stmt.setInt(9, pessoa.getId());
            int update = myCONN.executeUpdate(stmt);
            if (update == 1) {
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(PessoaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean updateStatus(Pessoa pessoa) {
        PreparedStatement stmt;
        try {
            stmt = myCONN.getConnection().prepareStatement("UPDATE Pessoa SET status=? WHERE id = ?");
            stmt.setInt(1, pessoa.getStatus());
            stmt.setInt(2, pessoa.getId());
            int update = myCONN.executeUpdate(stmt);
            if (update == 1) {
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(PessoaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public void delete(Pessoa pessoa) {
        PreparedStatement stmt;
        try {
            stmt = myCONN.getConnection().prepareStatement("DELETE FROM Pessoa WHERE id = ?");
            stmt.setInt(1, pessoa.getId());
            myCONN.executeUpdate(stmt);
        } catch (SQLException ex) {
            Logger.getLogger(PessoaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
