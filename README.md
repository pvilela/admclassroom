# README # - AdmClassroom
Author: Plinio Vilela - vilela at ydoo.com.br

This is a small Desktop Java project developed to organize the activities of a small (English and Computing) school in the classroom.

* Quick summary

[TODO]

* Version

0.9 - Initial Version
Still uses hand written DAO classes and a MySQL database.
Not suitable for general use yet. Basically created to initialize the Git repository and start the work to allow general use.

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)


### How do I get set up? ###
[TODO]
* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin: just email me: vilela at ydoo.com.br (Plinio Vilela)
* Other community or team contact: no one yet... :-(